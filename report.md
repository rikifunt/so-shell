% Report del progetto del corso "Sistemi Operativi"
% Andrea Fanti (mat. 1650746)

# Specifiche

Il progetto consiste in una shell testuale per sistemi POSIX e POSIX-like (ad
es. GNU/linux, macOS, ecc.) che supporti l'accesso remoto tramite un semplice
modello client/server. In particolare, la shell offre le seguenti funzionalità:

- un insieme minimale di comandi "builtin" che permettono di accedere e
  modificare le variabili d'ambiente e la directory di lavoro e di intercettare
  segnali diretti alla shell;

- creazione di processi sincroni alla shell a partire da eseguibili che siano in
  uno dei percorsi della variabile d'ambiente `PATH`;

- inoltro automatico di segnali rilevanti da client a server;

- inoltro automatico dell'I/O standard dei comandi da client a server (per
  l'input) e da server a client (per gli output); redirezione dell'I/O standard
  da/verso file presenti sulla macchina server;

- esecuzione batch (oltre all'esecuzione interattiva) di righe di comando
  contenute in file a piacere.

L'accesso remoto è realizzato con un architettura client/server, dove la parte
client si occupa di ricevere righe di comando dall'utente in maniera analoga a
quella delle shell testuali più comuni e la parte server si occupa di analizzare
ed eseguire le righe di comando ricevute mano mano dal client; la logica
principale è quindi implementata dal lato server in modo da snellire il client
il più possibile. Il lato server consiste nella pratica in un "demone" che
attende connessioni da nuovi client per poi creare un processo server vero e
proprio associato ad ogni nuovo client, di modo che un numero arbitrario di
sessioni siano possibili senza dover lanciare manualmente un processo server per
ogni sessione. Per lo scambio di informazioni tra client e server viene
utilizzato uno pseudo-protocollo che utilizza una singola connessione TCP per
ogni client.

# Implementazione

L'implementazione è divisa a grandi linee in due moduli indipendenti: la logica
principale, ovvero l'analisi ed esecuzione dei comandi, e l'accesso remoto,
ovvero lo scambio di linee di comando, inoltro dell'I/O standard, ecc.

## Implementazione della logica principale

La logica principale è interamente implementata dal lato server e si occupa
dell'analisi delle righe di comando e della loro esecuzione.

L'analisi dei comandi è eseguita dalla funzione `cmd_parse_line` che si occupa
prima l'analisi "lessicale" (divide le parti separate da spazi, gestisce
l'escaping, ecc.) e poi dell'analisi vera e propria, generando una `struct cmd`
che contiene il nome, gli argomenti del comando e gli eventuali percorsi ai file
da utilizzare al posto del terminale per l'I/O standard.

L'esecuzione dei comandi si basa su due strutture dati le cui istanze
rappresentano rispettivamente lo stato della shell e i comandi builtin.

La prima contiene il PID dell'eventuale processo figlio su cui è in esecuzione
un comando non builtin, le trap registrate, una coda di comandi (implementata
come lista collegata) da eseguire in seguito a trap e una flag che indica se la
sessione è terminata o meno. L'inzializzazione dello stato di una shell richiede
semplicemente di copiare la costante `shell_0` in una `struct shell` arbitraria.
La funzione `shell_register_event` si occupa di registrare l'avvenimento di
eventi per i quali si possono registrare trap e di mettere in coda nello stato
della shell eventuali comandi da eseguire.

L'esecuzione dei comandi è affidata alla funzione `core_exec`, che esegue un
dato comando, rappresentato da una `struct cmd`, nel contesto di una dato stato
della shell, fornitogli nella forma di una `struct shell`. I comandi builtin
sono memorizzati in `struct builtin`, che contengono nome, il numero di
argomenti attesi e il puntatore alla funzione "main" del comando.


## Implementazione dell'accesso remoto

L'accesso remoto è implementato tramite un protocollo minimale che utilizza una
singola connessione TCP (o di un qualsiasi tipo di connessione che permette lo
scambio affidabile di flussi di dati) per scambiare messaggi relativi ai comandi
da eseguire, al loro I/O, ecc.

Ogni messaggio è rappresentato da una `struct protocol_msg` ed è composto da:

- un byte che ne determina il tipo; gli unici valori validi sono definiti dalle
  costanti dichiarate in `protocol.h`;

- il payload, che può essere un byte o una stringa a seconda del tipo di
  messaggio; in quest'ultimo caso il payload è formato da un `int32_t` che
  indica la lunghezza dell'array in byte, seguito dall'array stesso.

Le funzioni `protocol_send_msg` e `protocol_recv_msg` permettono l'invio e la
ricezione bloccanti di messaggi in questo formato; eventuali interruzioni delle
chiamate interne a `read` e `write` non ne comportano il fallimento.

Una volta che il client ha stabilito la connessione con il server, lo scambio
dei messaggi deve avviene secondo il seguente ordine.

1. Il client stabilisce se la sessione è interattiva o meno e manda un messaggio
   `protocol_msg_session_mode` appropriato al server.

2. Il server manda a seconda dei casi:

   - un messaggio di tipo `protocol_msg_prompt` contenente la stringa (compreso
     il carattere nullo di terminazione) da mostrare all'utente come prompt
     durante la lettura della prossima riga di comando;

   - un messaggio di tipo `protocol_msg_pending_cmd` (il cui payload è ignorato)
     che indica che non va letta una nuova riga di comando perchè vi è un
     comando da eseguire a causa di una trap;

   - un messaggio di tipo `protocol_msg_exit_status` che indica che la sessione
     va terminata immediatamente\*.

3. Se ha ricevuto un messaggio `protocol_msg_prompt`, il client legge una riga
   di comando dall'utente; durante la lettura possono essere scambiati vari
   messaggi:

   - il server, o il client nel caso che la sessione non sia interattiva,
     possono mandare un messaggio di tipo `protocol_msg_exit_status` che indica
     che la sessione va terminata immediatamente\*;

   - il client può mandare un messaggio di tipo `protocol_msg_sig` che indica al
     server la ricezione di un segnale, indicato nel payload\*\*;

   - il server può mandare un messaggio di tipo `protocol_msg_pending_cmd` che
     indica che una trap ha causato l'esecuzione di un comando; in questo caso
     il client deve smettere di leggere la riga di comando corrente e passare
     alla fase successiva.

   Quando il client ha letto la riga, manda o un messaggio `protocol_msg_cmd_ln`
   contenente la riga letta come payload (senza il carattere newline finale ma
   con il carattere nullo di terminazione), oppure un messaggio di tipo
   `protocol_msg_eof`, che indica che l'utente ha inviato il carattere di EOF
   sul terminale; in questo ultimo caso, il server deve necessariamente
   rispondere con un messaggio di tipo `protocol_msg_exit_status` che termini la
   sessione immediatamente\*.

4. Il server inizia l'esecuzione del prossimo comando; vari messaggi possono
   essere scambiati, finchè l'I/O standard del comando non è riconosciuto come
   terminato:

   - il client può mandare un messaggio di tipo `protocol_msg_sig` che indica al
     server la ricezione di un segnale, indicato nel payload;

   - il server può mandare un messaggio di tipo `protocol_msg_exit_status` che
     indica che la sessione va terminata immediatamente\*;

   - il server può mandare messaggi di tipo `protocol_msg_out_ord` o
     `protocol_msg_out_diagn` che contengono come payload parte dell'output
     standard (rispettivamente ordinario e diagnostico) del comando in
     esecuzione (ovviamente l'ordine di questi messaggi deve rispettare l'ordine
     originale delle varie parti di output);

   - il server manda messaggi di tipo `protocol_msg_eof` con payload pari a 1 o
     2 per indicare che l'output standard rispettivamente ordinario o
     diagnostico del comando sono terminati; questi messaggi vanno inviati
     esattamente una volta per ogni esecuzione di un comando;

   - il client può mandare messaggi di tipo `protocol_msg_input` che contengono
     come payload parte dell'input standard letto dal client per il comando in
     esecuzione;

   - il client può mandare un singolo messaggio di tipo
     `protocol_msg_exit_status` per comunicare al server che la sessione deve
     essere terminata immediatamente\*.

   Quando l'output standard del comando (sia ordinario che diagnostico) è
   terminato il client deve smettere di leggere input dall'utente per il comando
   e deve mandare un messaggio di tipo `protocol_msg_eof` con payload pari a 0
   per indicare che l'input standard letto dall'utente per il comando è
   terminato; a questo punto, si ritorna al punto 2 finché la sessione non
   termina.

\* Ovvero la parte che riceve il messaggio di `protocol_msg_exit_status` deve
terminare immediatamente con exit status pari al payload del messaggio. Questo
avviene solitamente quando una delle due parti incontra un errore fatale che non
dipende dalla connessione o dall'altro lato.


# Utilizzo degli eseguibili

Per compilare entrambi gli eseguibili basta eseguire il comando `make` nella top
directory.

L'eseguibile del lato server (`so-shell-dem`) lancia il demone che attende
connessioni TCP sulla porta specificata come unico argomento; se non
specificata, viene usata la porta 9000.

Quando viene stabilita una connessione con un client, il demone crea un processo
figlio che si occuperà di portare avanti la sessione del client e si rimette in
attesa di ulteriori client. I processi figli vengono posti ognuno in un loro
gruppo di processi in modo che sia possibile mandare segnali al demone dal
terminale senza che vengano anche mandati ad eventuali processi figli in
esecuzione.

Il demone non adotta una gestione dei segnali particolare (a parte quella
necessaria a raccogliere lo status di terminazione di eventuali figli che
terminano); i suoi processi figli invece interpretano i segnali `SIGTERM`,
`SIGINT`, `SIGQUIT`, `SIGTSTP` e `SIGCONT` come rivolti ad eventuali comandi in
esecuzione (e sono quindi ignorati se non vi sono comandi in esecuzione);
segnali `SIGHUP` sono invece propagati ad eventuali comandi in esecuzione e
causano la terminazione (concordata col client) non appena essi terminano.

I possibili status di terminazione dei server sono:

- 0 dopo l'esecuzione di un comando `exit` (vedi sotto) o quando viene ricevuto
  un messaggio di `EOF` invece di una linea di comando dal client;

- 1 quando si verifica un errore fatale generico o al mancato rispetto del
  protocollo di una delle due parti;

- 2 quando la terminazione è dovuta a un segnale `SIGHUP`;

- 3 quando la terminazione è dovuta ad un errore relativo alla connessione (ad
  es. una delle due parti chiude la connessione inaspettatamente).

- Un server può inoltre terminare con un exit status uguale a quello del client
  associato quando si verifica un errore fatale nel client durante l'esecuzione
  di un comando; in questo caso, tuttavia, se avviene un errore nella ricezione
  dell'ultimo messaggio del client, il server terminerà comunque con exit status
  pari a 3.

L'eseguibile lato client (`so-shell-cli`) riceve al massimo 3 argomenti, tutti
opzionali. Se l'ultimo argomento è il percorso di un file esistente e leggibile
viene sempre trattato come il percorso di un file da eseguire come script\*; i
rimanenti due argomenti specificano rispettivamente l'indirizzo IP (in notazione
"dot") e la porta TCP del demone server (che deve essere già in attesa di
connessioni); se viene fornito un solo argomento o nessun argomento, viene
utilizzato l'indirizzo IP 127.0.0.1 (ovvero l'indirizzo "localhost"), mentre
l'eventuale unico argomento specifica la porta, che altrimenti è considerata
essere la 9000. Ad esempio, per specificare come porta la 9001 e come script il
file `prova.so-sh`:

`./so-shell-c 9001 prova.so-sh`

Per specificare come indirizzo `100.100.100.100`, come porta 9001 e come script
`prova.so-sh`:

`./so-shell-c 100.100.100.100 9001 prova.so-sh`

Per specificare come porta la 9001:

`./so-shell-c 9001`


Una volta stabilita la connessione col lato server, viene stampato il prompt e
l'utente può inserire comandi fin quando non termina la sessione. Per l'editing
della linea di comando in modalità interattiva viene usata la libreria GNU
readline; è stata utilizzata una funzione di completamento diversa da quella di
default per permettere il completamento dei comandi builtin.

L'unica differenza tra l'esecuzione interattiva e l'esecuzione batch è che
durante una sessione interattiva, se una riga di comando contiene un errore di
sintassi, la sessione continua, mentre se una riga di uno script contiene un
errore di sintassi la sessione viene terminata immediatamente.

I segnali `SIGTERM`, `SIGINT`, `SIGQUIT`, `SIGTSTP` e `SIGCONT` vengono
inoltrati al lato server se vi è un comando in esecuzione e vengono ignorati
altrimenti; segnali `SIGHUP` vengono sempre inoltrati al server.

I possibili status di terminazione dei client sono uguali a quelli dei server
(vedi sopra); in particolare, se un server invia correttamente il suo ultimo
messaggio ma il client associato incontra un errore nel riceverlo, allora il
client terminerà comunque con exit status pari a 3.


## Sintassi e semantica dei comandi

In questa sezione, il termine "shell" si riferisce al processo in cui una
sessione è in esecuzione. I percorsi relativi sono intesi come relativi alla
directory di lavoro della shell ove non specificato. Per "stampare" si intende
la stampa su `stdout`.

Ogni riga corrisponde a un singolo comando. Tutti le righe di comando non vuote
hanno la forma:

**`CMD`** `[`*`ARG`*`..]` `[<` *`INFILE`*`] [>` *`OUTFILE`*`] [>>` *`ERRFILE`*`]`

Dove:

- **`CMD`** è il nome del comando, che identifica il tipo di comando o, nel caso
  dei comandi di creazione di processi in foreground, il nome del file
  dell'eseguibile da caricare nel processo creato; *`ARG..`* è una lista
  (opzionale) di argomenti separati da spazi da passare al comando.

- *`INFILE`*, *`OUTFILE`* e *`ERRFILE`* sono percorsi (opzionali) ai file che
  vanno utilizzati, invece del terminale, come file per l'I/O standard.

La maggior parte dei comandi ha delle restrizioni sulla sintassi dei loro
argomenti che sono documentate nelle rispettive sezioni; tuttavia, ogni comando
fallisce (senza fare nulla) con un exit status pari a 1 se il numero degli
argomenti è sbagliato (e ciò non è documentato per ogni comando per brevità).

Tutti i comandi builtin ignorano i segnali `SIGTERM`, `SIGINT`, `SIGQUIT`,
`SIGTSTP`, `SIGCONT` e `SIGHUP` dato che non sono interattivi e il loro tempo di
esecuzione è trascurabile.

Precedere un qualsiasi carattere in una riga di comando con un carattere "`\`"
fa sì che un suo eventuale ruolo speciale (ad es. per i caratteri "`<`" e "`>`")
venga meno; questo vale anche per caratteri che non hanno significati
particolari, cosicché l'unico modo di scrivere un carattere backslash singolo
come carattere normale è "`\\`". Includere una parte della riga di comando tra
due caratteri "`"`" (doppi apici) fa sì che tutti i caratteri al suo interno
vengano trattati come normali come se fossero stati preceduti da un "`\`".


### Comandi di base

-   **`exit`**

    Stampa "exit" e termina la sessione corrente.

    * Exit status: nessuno.

-   **`getcwd`**

    Stampa il percorso assoluto della directory di lavoro della shell.

    * Exit status: 1 se la chiamata interna a `getcwd` fallisce; 0 altrimenti.

-   **`chdir`** *`DIR`*

    Imposta la directory di lavoro della shella a *`DIR`*, che è interpretato come
    un percorso assoluto se inizia per "`/`" e relativo altrimenti.

    * Exit status: 1 se la chiamata interna a `chdir` fallisce; 0 altrimenti.

### Comandi di accesso e manipolazione delle variabili d'ambiente

-   **`getenv`** *`VAR`*

    Stampa il valore della variabile d'ambiente di nome *`VAR`*.

    * Exit status: 1 se non c'è nessuna variabile denominata *`VAR`*
      nell'ambiente della shell e 0 altrimenti.

-   **`setenv`** *`VAR`* *`VAL`*

    Imposta il valore della variabile d'ambiente denominata *`VAR`* a *`VAL`*;
    se non c'è nessuna variabile denominata *`VAR`* nell'ambiente della shell,
    la aggiunge all'ambiente con *`VAL`* come valore. *`VAL`* non può contenere
    il carattere '='.

    * Exit status: 1 se *`VAL`* contiene un carattere '=' o se la chiamata
      interna a `setenv` fallisce per altri motivi; 0 altrimenti.

-   **`unsetenv`** *`VAR`*

    Rimuovi la variabile denominata *`VAR`* dall'ambiente della shell. *`VAR`*
    non può contenere un carattere '='.

    * Exit status: 1 se *`VAR`* contiene un carattere '='; 0 altrimenti.

### Comandi di gestione degli eventi

-   **`trap`** *`CMD`* *`EVENT`*..

    Registra *`CMD`* come trap per ogni *`EVENT`*, ovvero esegue *`CMD`* ogni
    volta che uno degli *`EVENT`* si verifica; eventuali trap precedentemente
    registrate per ogni *`EVENT`* vengono eliminate. *`CMD`* può essere una riga
    di comando o una stringa composta esclusivamente da caratteri di
    spaziatura\*; nell'ultimo caso, nessun comando verrà eseguito quando uno
    degli *`EVENT`* si verifica, per cui il risultato sarà semplicemente di
    eleminare ogni precedente trap registrata per ogni *`EVENT`*. *`EVENT`* può
    essere specificato come:

    - un segnale (in minuscolo), senza il prefisso `sig`, che indica che *`CMD`*
      deve essere eseguito quando la shell riceve il segnale specificato; i
      valori supportati sono: `term`,` int`, `quit`,` tstp`, `cont`,` hup`,
      `usr1` e `usr2`;

    - la stringa `debug`, che fa in modo che la shell esegua *`CMD`* dopo ogni
      comando eseguito in modo interattivo (ovvero non a seguito di un evento);

    - la stringa `exit`, che fa in modo che la shell esegua *`CMD`* prima di
      terminare (a meno che la terminazione non sia dovuta a connessione col
      client compromessa).

    Se un evento per il quale deve essere eseguito un comando si verifica mentre
    un altro il comando è in esecuzione, il comando verrà eseguito non appena il
    comando termina. Si noti che le trap registrate sono indipendenti dalla
    gestione dei segnali della shell stessa: registrare una trap per un segnale
    che la shell gestisce in qualche modo non ne cambierà la gestione, ma
    semplicemente la "arricchirà".

    \* I caratteri di spaziatura sono: spazio, "newline", tabulature orizzontali
       e verticali, "carriage return" e "line feed".

    * Exit status: 1 se non vi è sufficente memoria per registrare la trap, o
      se un *`EVENT`* non è valido; 0 altrimenti.


### Comandi di crazione di processi sincroni

Un processo sincrono è un processo figlo della shell del quale essa attende la
terminazione prima di stampare un nuovo prompt e consentire all'utente di
eseguire ulteriori comandi.

-   *`EXE`* `[`*`ARG`*`..]`

    Crea un processo sincrono dall'eseguibile *`EXE`*; il primo argomento
    passato all'eseguibile è *`EXE`* stesso, mentre gli argomenti successivi
    sono gli eventuali *`ARG`*.. passati. *`EXE`* viene interpretato come nelle
    chiamate a `execvp`, e l'ambiente del processo creato è ereditato dalla
    shell; più in generale, questo comando corrisponde a grandi linee ad una
    chiamata `fork` seguita da una chiamata `execvp` nel ramo figlio e una
    chiamata `wait` nel ramo genitore.

    * Exit status: 1 se la creazione del processo o altre operazioni relative
      alla gestione dell'I/O del processo falliscono; l'exit status del
      processo creato altrimenti.
