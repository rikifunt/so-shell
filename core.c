#define _POSIX_C_SOURCE 200112L // to make 'setenv' available
#include "core.h"

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
#include <assert.h>

#include "prelude.h"


// lexer/tokenizer
// ---------------

enum token_t
  { token_stdin_delim = 0
  , token_stdout_delim
  , token_stderr_delim
  , token_arg
  };

struct token
  { enum token_t type
  ; char*        content
  ;};

// Split the given command line 's_' into a list of tokens returns as the second
// and third argument. In case of errors, 'e[0]' will contain the line at which
// the error occured and 'e[1]' the possible associated value of 'errno'.
static outcome_t tokenize ( const char* s_
                          , int* toks_len_, struct token** toks_
                          , int e[2]
                          )
{
  int           s_len;
  // the provided string with each double-quote-enclosed part substituted by
  // backslash escapes of all its characters
  char*         s;
  int           toks_capacity;
  struct token* toks;
  // the indexes of, respectively, the next character to be processed in the
  // string and the next token to be parsed in the token array
  int           i, j;
  // Has one element for each character of 's'; each element is 0 if the
  // character must be discarded from the string when copying tokens, 1 if it
  // must be copied, 2 if it must be copied and is backslash-escaped.
  int*          s_flags;

  // Substitute double-quote-enclosed parts of the string with backslash escapes
  // of every character inside the double quotes.
  s = malloc (2 * (1+strlen (s_)));
  if (s == NULL) return (e[0] = __LINE__, e[1] = errno, failure);
  s_len = 0;
  for (unsigned n = 0, is_inside_double_quotes = 0; n < strlen (s_); n++)
  {
    if (s_[n] == '"' && !(!is_inside_double_quotes && n > 0 && s_[n-1] == '\\'))
      is_inside_double_quotes = !is_inside_double_quotes;
    else
    {
      if (is_inside_double_quotes)
      {
        s[s_len] = '\\';
        s_len++;
        s[s_len] = s_[n];
        s_len++;
      }
      else
      {
        s[s_len] = s_[n];
        s_len++;
      }
    }
  }
  s[s_len] = 0;

  s_flags = malloc (strlen (s) * sizeof *s_flags);
  if (s_flags == NULL) return (e[0] = __LINE__, e[1] = errno, failure);
  for (unsigned n = 0; n < strlen (s); n++)
    s_flags[n] = 1;

  // Split the resulting 's' into tokens.

  toks_capacity = 4;
  toks = malloc (toks_capacity * sizeof *toks);
  if (toks == NULL)
    return (free (s), free (s_flags), e[0] = __LINE__, e[1] = errno, failure);

  // Each iteration consumes a single token from 's'.
  i = j = 0;
  while (1)
  {
    // Skip the possible whitespace at the beginning of the next token; break
    // out of the outermost loop if there are no more tokens.
    while (s[i] != 0 && isspace (s[i]))
      i++;
    if (s[i] == 0)
      break;

    // Allocate memory for the next token; reallocate the whole array if it is
    // full.
    if (1+j > toks_capacity)
    {
      struct token* toks_old = toks;

      toks = realloc (toks, (toks_capacity *= 2) * sizeof *toks );
      if (toks == NULL)
      {
        for (int n = 0; n < j; n++)
          free (toks_old[n].content);
        free (toks_old);
        free (s);
        free (s_flags);
        e[0] = __LINE__;
        e[1] = errno;
        return failure;
      }
    }

    // Actually consume the token by identifying its type and then copying its
    // possible content.
    if (s[i] == '<' || s[i] == '>')
    {
      switch (s[i])
      {
        case '<': toks[j].type = token_stdin_delim; break;
        case '>':
          if (s[i+1] == '>')
          {
            toks[j].type = token_stderr_delim;
            i++;
          }
          else
            toks[j].type = token_stdout_delim;
          break;
        default: assert ("this execution path should be unreachale" == 0);
      }
      toks[j].content = NULL;
      i++;
    }
    else // i.e. the token is an argument or a command name
    {
      int k, unescaped_backslashes;

      toks[j].type = token_arg;
      for ( k = 0, unescaped_backslashes = 0
          ; s[i+k] != 0
            && ((!isspace (s[i+k]) && s[i+k] != '>' && s[i+k] != '<')
                || s_flags[i+k] == 2)
          ; k++
          )
      {
        if (s[i+k] == '\\' && s_flags[i+k] != 2)
        {
          s_flags[i+k] = 0;
          unescaped_backslashes++;
          if (s[i+k+1] != 0)
            s_flags[i+k+1] = 2;
        }
      }
      toks[j].content = malloc (1 + k - unescaped_backslashes);
      if (toks[j].content == NULL)
      {
        for (int n = 0; n < j; n++)
          free (toks[n].content);
        free (s);
        free (toks);
        free (s_flags);
        e[0] = __LINE__;
        e[1] = errno;
        return failure;
      }
      for (int n = 0, m = 0; n < k; n++)
        if (s_flags[i+n])
          toks[j].content[m] = s[i+n], m++;
      toks[j].content[k-unescaped_backslashes] = 0;

      i += k;
    }

    j++;
  }

  free (s);
  free (s_flags);

  *toks_     = toks;
  *toks_len_ = j;
  return success;
}


// parsing utilities and 'cmd_parse_line'
// --------------------------------------

// Parse an I/O redirection of the type given by 'tok_type' at the end of the
// given array of tokens; the (possible) resulting path is assigned to
// '*path_p'; the number returned is the number of consumed tokens, or -1 to
// indicate an error (none as of now).
static int parse_trailing_io_redirection
  (enum token_t tok_type, int toks_len, const struct token* toks, char** path_p)
{
  if (toks_len >= 3 && toks[toks_len-2].type == tok_type
      && toks[toks_len-1].type == token_arg)
  {
    *path_p = toks[toks_len-1].content;
    return 2;
  }
  else
    return 0;
}

// Generate an error message for a parse error. Only used by 'cmd_parse_line'.
static char* parse_strerror ( const char* ln, int unexpected_tok_type
                            , int e[2]
                            )
{
  const char* tok;
  const char  prefix[] = "parse error: unexpected I/O redirection token: \"";
  const char  infix[] = "  inside command line: \"";
  int         msg_len, n = 0;
  char*       msg;
  int         tok_len, ln_len;
  int         prefix_len = sizeof prefix - 1, infix_len = sizeof infix - 1;

  // NOTE: These rely on standard I/O redirection token types to
  //       evaluate to 0, 1 and 2.
  tok     = (const char*[]){ "<", ">", ">>" }[unexpected_tok_type];
  tok_len = (int[])        { 1  , 1  , 2}    [unexpected_tok_type];

  ln_len  = strlen (ln);

  // Accounts for:
  // - 2 newline characters,
  // - 2 double quote characters,
  // - the length of the prefix,
  // - the length of the unexpected token,
  // - the length of the infix,
  // - the length of the original command line.
  msg_len = 4 + prefix_len + tok_len + infix_len + ln_len;
  msg = malloc (1 + msg_len);
  if (msg == NULL) return (e[0] = __LINE__, e[1] = errno, NULL);

  for (int i = 0; i < prefix_len; i++, n++)
    msg[n] = prefix[i];
  for (int i = 0; i < tok_len; i++, n++)
    msg[n] = tok[i];
  msg[n++] = '\"';
  msg[n++] = '\n';

  for (int i = 0; i < infix_len; i++, n++)
    msg[n] = infix[i];
  for (int i = 0; i < ln_len; i++, n++)
    msg[n] = ln[i];
  msg[n++] = '\"';
  msg[n++] = '\n';

  msg[n] = 0;

  return msg;
}

outcome_t cmd_parse_line ( const char* ln, struct cmd* cmd
                         , int e[2], char** err_msg
                         )
{
  outcome_t     outcome;
  int           toks_len, toks_len_;
  struct token* toks_;
  struct token* toks;

  outcome = tokenize (ln, &toks_len, &toks, e);
  if (outcome == failure) return (*err_msg = NULL, failure);
  toks_     = toks;
  toks_len_ = toks_len;

  cmd->stdio_file_paths[0] = cmd->stdio_file_paths[1] = cmd->stdio_file_paths[2]
    = NULL;

  toks_len -= parse_trailing_io_redirection ( token_stderr_delim, toks_len, toks
                                            , &cmd->stdio_file_paths[2]);
  toks_len -= parse_trailing_io_redirection ( token_stdout_delim, toks_len, toks
                                            , &cmd->stdio_file_paths[1]);
  toks_len -= parse_trailing_io_redirection ( token_stdin_delim, toks_len, toks
                                            , &cmd->stdio_file_paths[0]);

  cmd->argc = toks_len;
  cmd->argv = malloc (cmd->argc * sizeof *(cmd->argv));
  if (cmd->argv == NULL)
  {
    for (int i = 0; i < toks_len_; i++)
      free (toks_[i].content);
    free (toks_);
    return (e[0] = __LINE__, e[1] = errno, *err_msg = NULL, failure);
  }
  int i;
  for (i = 0; i < cmd->argc && toks[i].type == token_arg; i++)
    cmd->argv[i] = toks[i].content;
  if (i < cmd->argc)
  {
    e[0] = __LINE__;
    e[1] = 0;
    *err_msg = parse_strerror (ln, toks[i].type, e);
    for (int j = 0; j < toks_len_; j++)
      free (toks_[j].content);
    free (toks_);
    free (cmd->argv);
    return failure;
  }

  free (toks_);

  return success;
}

struct shell shell_0 =
  { .has_exited        = 0
  , .child_pid         = 0
  , .traps             = {{ { 0 }, 0, NULL }}
  , .pending_cmds      = NULL
  , .pending_cmds_last = NULL
  };


// utility functions for copying and freeing command fields
// --------------------------------------------------------

outcome_t cmd_cpy_innards ( struct cmd* target, const struct cmd* source
                          , int e[2])
{
  assert (target != NULL && source != NULL && e != NULL);

  memset (target, 0, sizeof *target);
  if (source->argc == 0)
    return success;

  for (int i = 0; i < 3; i++)
  {
    if (source->stdio_file_paths[i] != NULL)
    {
      target->stdio_file_paths[i] =
        malloc (1 + strlen (source->stdio_file_paths[i]));
      if (target->stdio_file_paths[i] == NULL)
        return ( cmd_free_innards (target), e[0] = __LINE__, e[1] = errno
               , failure);
      strcpy (target->stdio_file_paths[i], source->stdio_file_paths[i]);
    }
  }

  target->argc = source->argc;
  target->argv = malloc (source->argc * sizeof *target->argv);
  if (target->argv == NULL)
    return ( cmd_free_innards (target), e[0] = __LINE__, e[1] = errno
           , failure);
  for (int i = 0; i < source->argc; i++)
  {
    target->argv[i] =
      malloc (1 + strlen (source->argv[i]));
    if (target->argv[i] == NULL)
      return ( cmd_free_innards (target), e[0] = __LINE__, e[1] = errno
             , failure);
    strcpy (target->argv[i], source->argv[i]);
  }

  return success;
}

void cmd_free_innards (struct cmd* cmd)
{
  assert (cmd != NULL);

  if (cmd->argc == 0)
    return;

  for (int i = 0; i < 3; i++)
    free (cmd->stdio_file_paths[i]);
  for (int i = 0; i < cmd->argc; i++)
    free (cmd->argv[i]);
  free (cmd->argv);
}


// registering trap events and queueing commands
// ---------------------------------------------

// Enqueue a single command triggered by a trap into the given shell state.
static outcome_t queue_cmd (struct shell* sh, const struct cmd* cmd, int e[2])
{
  struct cmds* this_cmds;

  assert (sh != NULL && cmd != NULL && e != NULL);

  this_cmds = malloc (sizeof *this_cmds);
  if (this_cmds == NULL)
    return (e[0] = __LINE__, e[1] = errno, failure);

  this_cmds->t = NULL;
  memcpy (&this_cmds->h, cmd, sizeof this_cmds->h);

  if (sh->pending_cmds == NULL)
    sh->pending_cmds = sh->pending_cmds_last = this_cmds;
  else
  {
    sh->pending_cmds_last->t = this_cmds;
    sh->pending_cmds_last    = this_cmds;
  }

  return success;
}

// NOTE: keep these non-zero and unoverlapping with POSIX signums.
int shell_evt_exit  = -1;
int shell_evt_debug = -2;

outcome_t shell_register_event (struct shell* sh, int evtnum, int e[2])
{
  outcome_t outcome;
  // NOTE: keep this "in sync" with the map in 'traps_main'.
  const int traps_ix_evtnum_map[] =
    { SIGTERM
    , SIGINT
    , SIGQUIT
    , SIGTSTP
    , SIGCONT
    , SIGHUP
    , SIGUSR1
    , SIGUSR2
    , shell_evt_exit
    , shell_evt_debug
    , 0 };

  assert (sh != NULL && e != NULL);

  for (int i = 0; traps_ix_evtnum_map[i] != 0; i++)
  {
    if (traps_ix_evtnum_map[i] == evtnum && sh->traps[i].argc > 0)
    {
      outcome = queue_cmd (sh, &sh->traps[i], e);
      assert (outcome == success);
    }
  }

  return success;
}

int shell_is_cmd_pending (struct shell* sh)
{
  assert (sh != NULL);

  return sh->pending_cmds != NULL;
}

void shell_dequeue_pending_cmd (struct shell* sh, struct cmd* cmd)
{
  struct cmds* next_pending_cmds;

  assert (sh != NULL && cmd != NULL);
  assert (shell_is_cmd_pending (sh));

  next_pending_cmds = sh->pending_cmds;

  memcpy (cmd, &next_pending_cmds->h, sizeof *cmd);

  sh->pending_cmds = sh->pending_cmds->t;
  free (next_pending_cmds);
}


// main functions for builtin commands
// -----------------------------------

typedef struct shell shell;

//TODO support first argument as exit status of the shell
static int exit_main (shell* sh, FILE* stdio_fs[], int argc, char* argv[])
{
  fprintf (stdio_fs[1], "exit\n");

  sh->has_exited = 1;

  return 0;
}

static int getcwd_main (shell* sh, FILE* stdio_fs[], int argc, char* argv[])
{
  //TODO refact in my style

  size_t n = 256;
  char *buf;

  while (1)
    {
      buf = malloc (n);
      if (buf == NULL)
      {
        fprintf (stdio_fs[2], "getcwd: insufficient virtual memory\n");
        return 1;
      }

      if (getcwd (buf, n) == buf)
        break;

      free (buf);
      if (errno != ERANGE)
      {
        fprintf (stdio_fs[2], "getcwd: %s\n", strerror (errno));
        return 1;
      }
      n *= 2;
    }

  fprintf (stdio_fs[1], "%s\n", buf);

  free (buf);

  return 0;
}

static int chdir_main (shell* sh, FILE* stdio_fs[], int argc, char* argv[])
{
  int outcome;

  outcome = chdir (argv[1]);
  if (outcome == -1)
  {
    fprintf (stdio_fs[2], "chdir: %s\n", strerror (errno));
    return 1;
  }

  return 0;
}

static int getenv_main (shell* sh, FILE* stdio_fs[], int argc, char* argv[])
{
  char *val;

  val = getenv (argv[1]);
  if (val == NULL)
  {
    fprintf (stdio_fs[2], "getenv: no variable with the specified name was"
                      " found in the environment\n");
    return 1;
  }

  fprintf (stdio_fs[1], "%s\n", val);
  return 0;
}

static int setenv_main (shell* sh, FILE* stdio_fs[], int argc, char* argv[])
{
  int outcome;

  outcome = setenv (argv[1], argv[2], 1);
  if (outcome == -1)
  {
    fprintf (stdio_fs[2], "setenv: %s\n", strerror (errno));
    return 1;
  }

  return 0;
}

static int unsetenv_main (shell* sh, FILE* stdio_fs[], int argc, char* argv[])
{
  int outcome;

  outcome = unsetenv (argv[1]);
  if (outcome == -1)
    {
      fprintf (stdio_fs[2], "unsetenv: the given environment variable name "
                       "contains a '='\n");
      return 1;
    }

  return 0;
}


static int trap_main (shell* sh, FILE* stdio_fs[], int argc, char* argv[])
{
  const char* const        evt_traps_ix_map[] =
    { "term"
    , "int"
    , "quit"
    , "tstp"
    , "cont"
    , "hup"
    , "usr1"
    , "usr2"
    , "exit"
    , "debug"
    };
  struct cmd               old_traps[10];
  int                      registered_traps[10] = { 0 };
  outcome_t                outcome;
  int                      est[2];
  struct cmd               cmd = { { NULL }, 0, NULL };

  if (argc < 3)
  {
    fprintf ( stdio_fs[2], "trap: error: too few arguments: "
                           "at least %i required, %i provided\n"
            , 3, argc);
    return 1;
  }

  if (!isempty (argv[1]))
  {
    char* parse_err_msg;

    outcome = cmd_parse_line (argv[1], &cmd, est, &parse_err_msg);
    if (outcome == failure)
    {
      fprintf (stdio_fs[2], "trap: so-shell: %s", parse_err_msg);
      free (parse_err_msg);
      return 1;
    }
  }

  memcpy (&old_traps[0], &sh->traps[0], 10 * sizeof old_traps[0]);

  for (int i = 2; i < argc; i++)
  {
    int j;

    for (j = 0; evt_traps_ix_map[j] != NULL; j++)
    {
      if (!registered_traps[j] && strcmp (argv[i], evt_traps_ix_map[j]) == 0)
      {
        outcome = cmd_cpy_innards (&sh->traps[j], &cmd, est);
        if (outcome == failure)
        {
          fprintf (stdio_fs[2], "trap: error: insufficient virtual memory\n");
          goto fail_cleanup;
        }
        registered_traps[j] = 1;
        break;
      }
    }
    if (evt_traps_ix_map[j] == NULL)
    {
      fprintf ( stdio_fs[2]
              , "trap: error: invalid event specification: %s\n", argv[i]);
      goto fail_cleanup;
    }
  }

  cmd_free_innards (&cmd);
  for (int k = 0; k < 10; k++)
    if (registered_traps[k])
      cmd_free_innards (&old_traps[k]);

  return 0;

fail_cleanup:
  cmd_free_innards (&cmd);
  for (int k = 0; k < 10; k++)
    if (registered_traps[k])
      cmd_free_innards (&sh->traps[k]);
  memcpy (&sh->traps[0], &old_traps[0], 10 * sizeof sh->traps[0]);
  return 1;
}

// a single builtin command // The 'main' functions should: use the provided
// streams as 'stdin', 'stdout' and 'stderr', respectively; use the last two
// arguments as 'argc' and 'argv' of C's usual main functions; return 0 or a
// positive integer as the exit status; set the 'has_exited' field of the shell
// state to indicate the session should be terminated (e.g. the 'exit' command).
struct builtin
  { const char* name
  ; int         expected_argc // if 0 means anything is valid
  ; int         (*main) (struct shell*, FILE*[], int, char*[])
  ;};

int core_exec (struct shell* sh, const struct cmd* cmd, int stdio_fds[])
{
  // map from command types to their builtin structures
  struct builtin builtins[] =
    { { .name = "exit", .expected_argc = 1, .main = exit_main }
    , { .name = "getcwd", .expected_argc = 1, .main = getcwd_main }
    , { .name = "chdir", .expected_argc = 2, .main = chdir_main }
    , { .name = "getenv", .expected_argc = 2, .main = getenv_main }
    , { .name = "setenv", .expected_argc = 3, .main = setenv_main }
    , { .name = "unsetenv", .expected_argc = 2, .main = unsetenv_main }
    , { .name = "trap", .expected_argc = 0, .main = trap_main }
    , { .name = NULL, .expected_argc = -1, .main = NULL }
    };
  struct builtin* cmd_builtin = NULL;
  int   exit_status;
  int   outcome;
  int   stdio_fds1[3];
  FILE* stdio_fs[3];

  for (int i = 0; i < 3; i++)
  {
    if (cmd->stdio_file_paths[i] != NULL)
    {
      stdio_fs[i] = fopen (cmd->stdio_file_paths[i], i == 0 ? "r" : "w");
      assert (stdio_fs[i] != NULL);

      stdio_fds1[i] = fileno (stdio_fs[i]);
    }
    else
    {
      stdio_fds1[i] = dup (stdio_fds[i]);
      assert (stdio_fds1[i] != -1);

      stdio_fs[i] = fdopen (stdio_fds1[i], i == 0 ? "r" : "w");
      assert (stdio_fs[i] != NULL);
    }
    outcome = setvbuf (stdio_fs[i], NULL, _IOLBF, 0);
    assert (outcome == 0);
  }

  for (int i = 0; builtins[i].name != NULL; i++)
    if (strcmp (cmd->argv[0], builtins[i].name) == 0)
      cmd_builtin = &builtins[i];
  if (cmd_builtin != NULL)
  {
    if (cmd_builtin->expected_argc > 0
        && cmd->argc != cmd_builtin->expected_argc)
    {
      fprintf ( stdio_fs[2]
              , "%s: error: wrong number of arguments: %i expected, %i provided\n"
              , *(cmd->argv), cmd_builtin->expected_argc-1, cmd->argc-1
              );
      exit_status = 1;
    }
    else
      exit_status = cmd_builtin->main (sh, stdio_fs, cmd->argc, cmd->argv);
  }
  else
  {
    sh->child_pid = fork ();
    if (sh->child_pid == 0)
    {
      for (int i = 0; i < 3; i++)
      {
        outcome = dup2 (stdio_fds1[i], i);
        assert (outcome == i);
      }

      char** argv;
      /* Append a NULL pointer at the end of 'argv' as 'execvp' requires it. */
      {
        argv = malloc (sizeof *argv * (1+cmd->argc));
        assert (argv != NULL);
        memcpy (argv, cmd->argv, sizeof *argv * cmd->argc);
        argv[cmd->argc] = NULL;
        free (cmd->argv);
      }
      outcome = execvp (argv[0], argv);
      if (outcome == -1)
      {
        perror ("execvp");
        exit (EXIT_FAILURE);
      }
    }
    else if (sh->child_pid > 0)
    {
      int status;

      do outcome = wait (&status);
      while (outcome == -1 && errno == EINTR);
      assert (outcome != -1);
      sh->child_pid = 0;
      exit_status = WEXITSTATUS (status);
    }
    else
    {
      fprintf (stdio_fs[2], "fork: %s\n", strerror (errno));
      exit_status = 1;
    }
  }

  for (int i = 0; i < 3; i++)
  {
    outcome = fclose (stdio_fs[i]);
    assert (outcome == 0);
  }

  return exit_status;
}
