A simple \*nix text-based shell supporting remote access with a server-client
architecture: the client process connects to a running server process to carry
out a single shell session; server processes are in turn spawned on-demand by a
server daemon that may run on a remote machine.

Informations (in italian) on building and running are found in "`report.md`".
