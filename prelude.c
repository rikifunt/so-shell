#include "prelude.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>


int isempty (char* cmd_ln)
{
  const char whitespace[] = " \t\v\r\f\n";
  int        i, is_empty = 1;

  for (i = 0; cmd_ln[i] != 0; i++)
  {
    int is_whitespace = 0;

    for (unsigned j = 0; j < sizeof whitespace; j++)
      if (cmd_ln[i] == whitespace[j])
        is_whitespace = 1;

    if (!is_whitespace)
    {
      is_empty = 0;
      break;
    }
  }

  return is_empty;
}



static const char eueof_strerror[] = "Connection closed by peer";

const char* prelude_strerror (int errno_val)
{
  if (errno_val == EUEOF)
    return eueof_strerror;
  else
    return strerror (errno_val);
}

outcome_t read_chars (int fd, ssize_t n, char* chars)
{
  ssize_t global_read = 0;
  ssize_t local_read  = 0;

  while (global_read < n)
  {
    do local_read = read (fd, chars + global_read, n - global_read);
    while (local_read == -1 && errno == EINTR);
    if (local_read == 0)
      return (errno = EUEOF, failure);
    else if (local_read == -1)
      return failure;

    global_read += local_read;
  }

  return success;
}

outcome_t write_chars (int fd, ssize_t n, const char* chars)
{
  ssize_t          global_written = 0;
  ssize_t          local_written  = 0;
  int              outcome;
  struct sigaction old_sa;
  struct sigaction ign_sigpipe_sa;

  ign_sigpipe_sa.sa_handler = SIG_IGN;
  ign_sigpipe_sa.sa_flags   = 0;
  sigemptyset (&ign_sigpipe_sa.sa_mask);
  outcome = sigaction (SIGPIPE, &ign_sigpipe_sa, &old_sa);
  assert (outcome == 0);

  while (global_written < n)
  {
    do local_written = write (fd, chars + global_written, n - global_written);
    while (local_written == -1 && errno == EINTR);
    if (local_written == -1)
    {
      outcome = sigaction (SIGPIPE, &old_sa, NULL);
      assert (outcome == 0);
      return failure;
    }

    global_written += local_written;
  }

  outcome = sigaction (SIGPIPE, &old_sa, NULL);
  assert (outcome == 0);

  return success;
}
