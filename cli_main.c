// main function for the client side of so-shell

#ifdef __linux__
#define _POSIX_C_SOURCE 1 // For 'sigprocmask' from glibc on Linux.
#endif

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "prelude.h"
#include "protocol.h"

#if FD_SETSIZE < 2
#error "Systems with FD_SETSIZE less than 2 are not supported"
#endif


// utilities for accurate error reporting
// --------------------------------------
// NOTE: variables named 'e' in this file are integer arrays of two elements
// storing respectively the line and the possible value of 'errno' of an error.

static void vdiagn (int line, int e[2], const char* msg_fmt, va_list args)
{
  if (e == NULL)
    fprintf (stderr, "so-shell-cli: error %i: ", line);
  else
    fprintf (stderr, "so-shell-cli: error %i.%i.%i: ", line, e[0], e[1]);

  vfprintf (stderr, msg_fmt, args);

  if (e != NULL && e[1] != 0)
    fprintf (stderr, ": %s", prelude_strerror (e[1]));

  fprintf (stderr, "\n");
}

static void diagn (int line, int e[2], const char* msg_fmt, ...)
{
  va_list args;

  va_start (args, msg_fmt);
  vdiagn (line, e, msg_fmt, args);
}

static void diagn_and_exit ( int exit_status, int line, int e[2]
                           , const char* msg_fmt, ...)
{
  if (exit_status != 0)
  {
    va_list args;

    va_start (args, msg_fmt);
    vdiagn (line, e, msg_fmt, args);
  }

  exit (exit_status);
}

static const char too_many_args_msgf[] =
                    "too many arguments: %i provided, at most 2 expected"
                , unrepr_port_msgf[] =
                    "the given port represents an integer not representable "
                    "in this implementation because of overflow: %s"
                , unparsbl_port_msgf[] =
                    "the given port does not represent an integer: %s"
                , invalid_port_msgf[] =
                    "the given port is not a valid TCP port: %s"
                , invalid_ip_addr_msgf[] =
                    "the given IP address is not a valid IP address or is not"
                    " written in standard dot notation: %s"
                , unexpected_type_msgf[] =
                    "the server did not follow the protocol: a message of an "
                    "unexpected type with message type id '%i' was received"
                , unexpected_payload_msgf[] =
                    "the server did not follow the protocol: unexpected "
                    "payload %i for a message of type %i"
                , socket_fail_msgf[] =
                    "failed to create the socket needed to connect to the "
                    "server"
                , connect_fail_msgf[] =
                    "failed to connect to the server at %s:%i"
                , recv_fail_msgf[] =
                    "failed to receive a message from the server"
                , send_fail_msgf[] =
                    "failed to send a message to the server"
                , display_out_fail_msgf[] =
                    "failed to display command output to the user"
                , failed_read_stdin_msgf[] =
                    "failed to read from standard input"
                , srv_fail_msgf[] =
                    "the server encountered a fatal error"
                , ignored_sig_msgf[] =
                    "signal number %i ignored due to full signal handling queue"
                , open_fail_msgf[] =
                    "failed to open the script file"
                , batch_read_failed[] =
                    "failed to read from the script file"
                ;


// utilities for queueing signals to be forwarded to the server side
// -----------------------------------------------------------------

static int sig_forwarding_queue_len = 0;
static int sig_forwarding_queue[8];

// Signal handler function that queues a single signal to be forwarded.
// NOTE: To avoid race conditions, ALL forwarded signals must be blocked inside
// this handler: this is delegated to the function that sets up this function as
// a signal handler (as of now, 'main').
static void queue_sig_forwarding (int signum)
{
  if (sig_forwarding_queue_len >= 8)
    diagn (__LINE__, NULL, ignored_sig_msgf, signum);
  else
    sig_forwarding_queue[sig_forwarding_queue_len++] = signum;
}

// Atomically (i.e. blocking appropriate signals) empty the signal queue.
static void empty_sig_forwarding_queue (sigset_t mask)
{
  int outcome;

  sigset_t old_procmask;

  outcome = sigprocmask ( SIG_SETMASK
                        , &mask
                        , &old_procmask);
  assert (outcome == 0);

  sig_forwarding_queue_len = 0;

  outcome = sigprocmask (SIG_SETMASK, &old_procmask, NULL);
  assert (outcome == 0);
}


// globals and functions to simplify the communcation with the server in 'main'
// ----------------------------------------------------------------------------

static int srv_sockd;

// utility to send the last message to the server before exiting due to a
// failure
static void goodbye_srv (int exit_status)
{
  outcome_t           outcome;
  // see the comment at the top of error reporting utilities for details
  int                 e[2];
  struct protocol_msg msg;

  msg.type      = protocol_msg_exit_status;
  msg.payload.c = exit_status;
  outcome = protocol_send_msg (srv_sockd, msg, e);
  if (outcome == failure)
    diagn_and_exit (3, __LINE__, e, send_fail_msgf);
}

static int is_editing_cmd_ln;
static int is_interactive = 1;


// functions and globals related to reading command lines
// ------------------------------------------------------

// Returns 1 if EOF was encountered without reading any characters and 0
// otherwise. The line is read from 'script_fd' into 'ln' and must be freed with
// 'free' after use. On errors, -1 is returned and the possible error is stored
// in 'e' (see the comment at the top of error reporting utilities for details).
static int non_interactive_read_ln (int script_fd, char** ln, int e[2])
{
  int   outcome;
  int   buf_capacity = 64, buf_len = 0;
  char* buf;

  buf = malloc (buf_capacity);
  if (buf == NULL) return (e[0] = __LINE__, e[1] = errno, -1);

  do
  {
    if (buf_len >= buf_capacity)
    {
      char* old_buf = buf;

      buf = realloc (buf, buf_capacity *= 2);
      if (buf == NULL)
        return (free (old_buf), e[0] = __LINE__, e[1] = errno, -1);
    }
    do outcome = read (script_fd, buf + buf_len, 1);
    while (outcome == -1 && errno == EINTR);
    if (outcome == 0)
    {
      if (buf_len == 0)
        return (free (buf), 1);
      else
        buf[buf_len] = '\n';
    }
    if (outcome == -1)
      return (e[0] = __LINE__, e[1] = errno, -1);
    buf_len++;
  }
  while (buf[buf_len-1] != '\n');

  buf[buf_len-1] = 0;
  *ln = buf;

  return 0;
}

// function used both to send command lines in non-interactive session and as
// the GNU readline callback in interactive sessions
// 'cmd_ln' is assumed to be dynamically allocated and is free'd inside this
// function.
static void send_cmd_ln (char* cmd_ln)
{
  // see the comment at the top of error reporting utilities for details
  int                 e[2];
  outcome_t           outcome;
  struct protocol_msg msg;

  if (cmd_ln == NULL) // i.e. EOF
  {
    msg.type      = protocol_msg_eof;
    msg.payload.c = 0;
    outcome = protocol_send_msg (srv_sockd, msg, e);
    if (outcome == failure)
      diagn_and_exit (3, __LINE__, e, send_fail_msgf);

    if (is_interactive)
      rl_callback_handler_remove ();

    outcome = protocol_recv_msg (srv_sockd, &msg, e);
    if (outcome == failure)
      diagn_and_exit (3, __LINE__, e, recv_fail_msgf);
    if (msg.type == protocol_msg_exit_status)
    {
      if (msg.payload.c != 0 && msg.payload.c != 2)
        diagn_and_exit (msg.payload.c, __LINE__, NULL, srv_fail_msgf);
      else
        exit (msg.payload.c);
    }
    else if (msg.type != protocol_msg_pending_cmd)
      diagn_and_exit (1, __LINE__, NULL, unexpected_type_msgf, msg.type);

    is_editing_cmd_ln = 0;
  }
  else if (!isempty (cmd_ln))
  {
    msg.type             = protocol_msg_cmd_ln;
    msg.payload.s.length = 1 + strlen (cmd_ln);
    msg.payload.s.head   = cmd_ln;
    outcome = protocol_send_msg (srv_sockd, msg, e);
    if (outcome == failure)
      diagn_and_exit (3, __LINE__, e, send_fail_msgf);

    if (is_interactive)
      add_history (cmd_ln);

    is_editing_cmd_ln = 0;

    if (is_interactive)
      rl_callback_handler_remove ();
  }

  free (cmd_ln);
}

//TODO get this from the server
static char* builtin_cmd_names[] =
  { "exit"
  , "getcwd"
  , "chdir"
  , "getenv"
  , "setenv"
  , "unsetenv"
  , "trap"
  , NULL
  };

// generator function for GNU readline custom completion
static char* builtin_cmd_name_generator (const char *text, int state)
{
  static int i, len;

  // If starting to complete a new word, initialize 'i' and the cached length of
  // 'text'.
  if (state == 0)
  {
    i = 0;
    len = strlen (text);
  }

  // Set 'i' to the next command name partially matching 'text', or to 'NULL' if
  // there are none.
  for (; builtin_cmd_names[i] != NULL; i++)
    if (strncmp (builtin_cmd_names[i], text, len) == 0)
      break;

  char* name_cpy = NULL;
  if (builtin_cmd_names[i] != NULL)
  {
    name_cpy = malloc (1 + strlen (builtin_cmd_names[i]));
    // If there is not enough memory, simply abort the completion instead of
    // failing.
    if (name_cpy == NULL)
      return NULL;
    strcpy (name_cpy, builtin_cmd_names[i]);
    i++;
  }

  return name_cpy;
}

// the readline custom completion function
static char** so_shell_rl_completion_func (const char *text, int start, int end)
{
  char** matches = NULL;

  // If at the start of the line, supply builtin commands completion.
  if (start == 0)
    matches = rl_completion_matches (text, builtin_cmd_name_generator);

  return matches;
}


int main (int argc, char* argv[])
{
  const int           forwarded_signums[] =
                        { SIGHUP
                        , SIGTERM
                        , SIGINT
                        , SIGQUIT
                        , SIGTSTP
                        , SIGCONT
                        , 0
                        };
  struct sigaction    queue_sig_forwarding_sa;
  outcome_t           outcome;
  // see the comment at the top of error reporting utilities for details
  int                 e[2];
  struct sockaddr_in  srv_addr;
  char*               script_path;
  int                 script_fd;
  struct protocol_msg msg;
  char*               prompt;

  // Initialize the signal action needed for custom signal handling.
  queue_sig_forwarding_sa.sa_handler = queue_sig_forwarding;
  queue_sig_forwarding_sa.sa_flags   = 0;
  sigemptyset (&queue_sig_forwarding_sa.sa_mask);
  for (int i = 0; forwarded_signums[i] != 0; i++)
  {
    outcome =
      sigaddset (&queue_sig_forwarding_sa.sa_mask, forwarded_signums[i]);
    assert (outcome == 0);
  }

  // Parse command line arguments.
  {
    const char* srv_ip_addr = "127.0.0.1";
    int16_t     srv_port    = 9000;

    // Parse the possible script file path CLI argument.
    script_path = NULL;
    if (argc > 1)
    {
      if (access (argv[argc-1], R_OK) == 0)
      {
        script_path = argv[argc-1];
        argc--;
      }
      else
        assert (errno != EFAULT && errno != EINVAL);
    }

    // Parse the possible port and IP address CLI arguments.

    if (argc == 2 || argc == 3)
    {
      char* port_str;
      char* leftover;
      int   port;

      port_str = argc == 2 ? argv[1] : argv[2];

      errno = 0;
      port = strtol (port_str, &leftover, 0);
      if (errno == ERANGE)
        diagn_and_exit (1, __LINE__, NULL, unrepr_port_msgf, port_str);
      else if (*leftover != 0)
        diagn_and_exit (1, __LINE__, NULL, unparsbl_port_msgf, port_str);
      else if (port > 65535 || port < 0)
        diagn_and_exit (1, __LINE__, NULL, invalid_port_msgf, port_str);

      srv_port = port;

      if (argc == 3)
        srv_ip_addr = argv[1];
    }
    else if (argc > 3)
      diagn_and_exit (1, __LINE__, NULL, too_many_args_msgf, argc-1);

    srv_addr.sin_addr.s_addr = inet_addr (srv_ip_addr);
    if (srv_addr.sin_addr.s_addr == (in_addr_t)-1)
      diagn_and_exit (1, __LINE__, NULL, invalid_ip_addr_msgf, srv_ip_addr);
    srv_addr.sin_family      = AF_INET;
    srv_addr.sin_port        = htons (srv_port);
  }

  // Open and connect 'srv_sockd' to the server.

  srv_sockd = socket (PF_INET, SOCK_STREAM, 0);
  if (srv_sockd == -1)
  {
    e[0] = 0;
    e[1] = errno;
    diagn_and_exit (1, __LINE__, e, socket_fail_msgf);
  }

  outcome = connect (srv_sockd, (struct sockaddr*)&srv_addr, sizeof srv_addr);
  if (outcome == -1)
  {
    e[0] = 0;
    e[1] = errno;
    diagn_and_exit ( 3, __LINE__, e, connect_fail_msgf
                   , inet_ntoa (srv_addr.sin_addr), ntohs (srv_addr.sin_port)
                   );
  }

  // Determine if the session is interactive or batch and possibly open the
  // script file for reading.
  is_interactive = script_path == NULL;
  if (!is_interactive)
  {
    script_fd = open (script_path, O_RDONLY);
    if (script_fd == 1)
      diagn (__LINE__, (int[2]){0, errno}, open_fail_msgf);
  }

  // Setup signal handling.
  for (int i = 0; forwarded_signums[i] != 0; i++)
  {
    outcome = sigaction (forwarded_signums[i], &queue_sig_forwarding_sa, NULL);
    assert (outcome == 0);
  }

  // Setup GNU readline.
  if (is_interactive)
  {
    //TODO is this needed?
    //rl_catch_signals = 0;
    rl_attempted_completion_function = so_shell_rl_completion_func;
  }

  // protocol step 1: determining session interactivity
  msg.type      = protocol_msg_session_mode;
  msg.payload.c = is_interactive ? 1 : 0;
  outcome = protocol_send_msg (srv_sockd, msg, e);
  if (outcome == failure)
    diagn_and_exit (3, __LINE__, e, send_fail_msgf);

  while (1)
  {
    // protocol step 2

    outcome = protocol_recv_msg (srv_sockd, &msg, e);
    if (outcome == failure)
      diagn_and_exit (3, __LINE__, e, recv_fail_msgf);
    if (msg.type == protocol_msg_exit_status)
    {
      //TODO don't call this on the first iteration
      if (is_interactive)
        rl_callback_handler_remove ();
      if (msg.payload.c != 0 && msg.payload.c != 2)
        diagn_and_exit (msg.payload.c, __LINE__, NULL, srv_fail_msgf);
      else
        exit (msg.payload.c);
    }
    else if (msg.type == protocol_msg_prompt)
    {
      // protocol step 3: line editing

      prompt = msg.payload.s.head;

      if (is_interactive)
        rl_callback_handler_install (prompt, send_cmd_ln);
      is_editing_cmd_ln = 1;
      while (is_editing_cmd_ln)
      {
        fd_set         ln_ed_fds;
        struct timeval zero_tv = { .tv_sec = 0, .tv_usec = 0 };

        // Forward queued signals (that were queued during command line
        // editing).
        if (sig_forwarding_queue_len > 0)
        {
          sigset_t old_procmask;

          outcome = sigprocmask ( SIG_SETMASK
                                , &queue_sig_forwarding_sa.sa_mask
                                , &old_procmask
                                );
          assert (outcome == 0);

          for (int i = 0; i < sig_forwarding_queue_len; i++)
          {
            msg.type      = protocol_msg_sig;
            msg.payload.c = protocol_sigid (sig_forwarding_queue[i]);
            outcome = protocol_send_msg (srv_sockd, msg, e);
            if (outcome == failure)
              diagn_and_exit (3, __LINE__, e, send_fail_msgf);
          }
          sig_forwarding_queue_len = 0;

          outcome = sigprocmask (SIG_SETMASK, &old_procmask, NULL);
          assert (outcome == 0);
        }

        // Check for input either from the server or from the user.
        FD_ZERO   (              &ln_ed_fds);
        FD_SET    (srv_sockd,    &ln_ed_fds);
        if (is_interactive)
          FD_SET  (STDIN_FILENO, &ln_ed_fds);
        do outcome = select (FD_SETSIZE, &ln_ed_fds, NULL, NULL, &zero_tv);
        while (outcome == -1 && (errno == EINTR || errno == EAGAIN));
        assert (outcome != -1);

        // Handle messages the server could have sent during step 3 of the
        // protcol.
        if (FD_ISSET (srv_sockd, &ln_ed_fds))
        {
          outcome = protocol_recv_msg (srv_sockd, &msg, e);
          if (outcome == failure)
            diagn_and_exit (3, __LINE__, e, recv_fail_msgf);
          if (msg.type == protocol_msg_exit_status)
          {
            if (is_interactive)
              rl_callback_handler_remove ();
            if (msg.payload.c != 0 && msg.payload.c != 2)
              diagn_and_exit (msg.payload.c, __LINE__, NULL, srv_fail_msgf);
            else
              exit (msg.payload.c);
          }
          else if (msg.type == protocol_msg_pending_cmd)
          {
            if (is_interactive)
              rl_callback_handler_remove ();
            break;
          }
          else
            diagn_and_exit (1,__LINE__, NULL, unexpected_type_msgf, msg.type);
        }

        // If the session is interactive and there is input from the user, call
        // the GNU readline callback; otherwise, read a line from the script.
        if (is_interactive && FD_ISSET (STDIN_FILENO, &ln_ed_fds))
          rl_callback_read_char ();
        else if (!is_interactive)
        {
          char* ln;
          int   is_eof;

          is_eof = non_interactive_read_ln (script_fd, &ln, e);
          if (is_eof == -1)
          {
            goodbye_srv (1);
            diagn_and_exit (1, __LINE__, e, batch_read_failed);
          }
          send_cmd_ln (is_eof ? NULL : ln);
        }
      }
      free (prompt);

      empty_sig_forwarding_queue (queue_sig_forwarding_sa.sa_mask);
    }
    else if (msg.type != protocol_msg_pending_cmd)
      diagn_and_exit (1, __LINE__, NULL, unexpected_type_msgf, msg.type);

    // protocol step 4: command execution
    int is_stdin_open = 1, is_ord_terminated = 0, is_diagn_terminated = 0;
    do
    {
      fd_set         cmd_io_fds;
      struct timeval zero_tv = { .tv_sec = 0, .tv_usec = 0 };

      // Forward queued signals (that were queued during the execution of the
      // current command).
      if (sig_forwarding_queue_len > 0)
      {
        sigset_t old_procmask;

        outcome = sigprocmask ( SIG_SETMASK
                              , &queue_sig_forwarding_sa.sa_mask
                              , &old_procmask);
        assert (outcome == 0);

        for (int i = 0; i < sig_forwarding_queue_len; i++)
        {
          msg.type      = protocol_msg_sig;
          msg.payload.c = protocol_sigid (sig_forwarding_queue[i]);
          outcome = protocol_send_msg (srv_sockd, msg, e);
          if (outcome == failure)
            diagn_and_exit (3, __LINE__, e, send_fail_msgf);
        }
        sig_forwarding_queue_len = 0;

        outcome = sigprocmask (SIG_SETMASK, &old_procmask, NULL);
        assert (outcome == 0);
      }

      // Check for command output forwarded from the server (or other messages)
      // or for input from the user.
      FD_ZERO  (              &cmd_io_fds);
      FD_SET   (srv_sockd,    &cmd_io_fds);
      if (is_stdin_open)
        FD_SET (STDIN_FILENO, &cmd_io_fds);
      do outcome = select (FD_SETSIZE, &cmd_io_fds, NULL, NULL, &zero_tv);
      while (outcome == -1 && (errno == EINTR || errno == EAGAIN));
      assert (outcome != -1);

      //TODO user should be able to choose buffering strategy? (current is
      //bounded-length buffering since the terminal already buffers by line)
      // Handle input from the user.
      if (is_stdin_open && FD_ISSET (STDIN_FILENO, &cmd_io_fds))
      {
        int       buf_len;
        const int buf_capacity = 128;
        char      buf[buf_capacity];

        do buf_len = read (STDIN_FILENO, buf, buf_capacity);
        while (buf_len == -1 && errno == EINTR);
        assert (buf_len != -1 || (buf_len == -1 && errno == EIO));
        if (buf_len == -1)
        {
          e[0] = 0;
          e[1] = errno;
          goodbye_srv (1);
          diagn_and_exit (1, __LINE__, e, failed_read_stdin_msgf);
        }
        if (buf_len == 0)
        {
          is_stdin_open = 0;
          msg.type      = protocol_msg_eof;
          msg.payload.c = 0;
          outcome = protocol_send_msg (srv_sockd, msg, e);
          if (outcome == failure)
            diagn_and_exit (3, __LINE__, e, send_fail_msgf);
        }
        else
        {
          msg.type             = protocol_msg_input;
          msg.payload.s.length = buf_len;
          msg.payload.s.head   = &(buf[0]);
          outcome = protocol_send_msg (srv_sockd, msg, e);
          if (outcome == failure)
            diagn_and_exit (3, __LINE__, e, send_fail_msgf);
        }
      }

      // Handle messages from the server.
      if (FD_ISSET (srv_sockd, &cmd_io_fds))
      {
        outcome = protocol_recv_msg (srv_sockd, &msg, e);
        if (outcome == failure)
          diagn_and_exit (3, __LINE__, e, recv_fail_msgf);
        if (msg.type == protocol_msg_eof)
        {
          if (msg.payload.c == 1)
            is_ord_terminated = 1;
          else if (msg.payload.c == 2)
            is_diagn_terminated = 1;
          else
            diagn_and_exit ( 1, __LINE__, NULL, unexpected_payload_msgf
                           , msg.payload.c, msg.type );
        }
        else if (msg.type == protocol_msg_out_ord
                 || msg.type == protocol_msg_out_diagn)
        {
          outcome = write_chars ( msg.type == protocol_msg_out_ord
                                   ? STDOUT_FILENO
                                   : STDERR_FILENO
                                , msg.payload.s.length, msg.payload.s.head
                                );
          if (outcome == failure)
          {
            e[0] = 0;
            e[1] = errno;
            goodbye_srv (1);
            //TODO unless we ensure to read all pending data from srv_sockd,
            //this may result in an ECONNRESET on the server side instead of a
            //coop exit
            diagn_and_exit (1, __LINE__, e, display_out_fail_msgf);
          }
          free (msg.payload.s.head);
        }
        else if (msg.type == protocol_msg_exit_status)
        {
          if (msg.payload.c != 0 && msg.payload.c != 2)
            diagn_and_exit (msg.payload.c, __LINE__, NULL, srv_fail_msgf);
          else
            exit (msg.payload.c);
        }
        else
          diagn_and_exit (1, __LINE__, NULL, unexpected_type_msgf, msg.type);
      }
    }
    while (!is_ord_terminated || !is_diagn_terminated);

    // Properly end the step 4 of the protocol.
    if (is_stdin_open)
    {
      msg.type      = protocol_msg_eof;
      msg.payload.c = 0;
      outcome = protocol_send_msg (srv_sockd, msg, e);
      if (outcome == failure)
        diagn_and_exit (3, __LINE__, e, send_fail_msgf);
    }
  }
}
