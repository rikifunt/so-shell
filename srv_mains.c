// main function(s) for the server side(s) of so-shell

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <errno.h>
#include <assert.h>

#include "prelude.h"
#include "core.h"
#include "protocol.h"

#if FD_SETSIZE < 1
#error "Systems with FD_SETSIZE less than 1 are not supported"
#endif


// utilities for accurate error reporting
// --------------------------------------
// NOTE: variables named 'e' in this file are integer arrays of two elements
// storing respectively the line and the possible value of 'errno' of an error.

static void diagn ( const char* prog_name, int line, int e[2]
                  , const char* msg_fmt, va_list args
                  )
{
  if (e == NULL)
    fprintf (stderr, "%s: error %i: ", prog_name, line);
  else
    fprintf (stderr, "%s: error %i.%i.%i: ", prog_name, line, e[0], e[1]);

  vfprintf (stderr, msg_fmt, args);

  if (e != NULL && e[1] != 0)
    fprintf (stderr, ": %s", prelude_strerror (e[1]));

  fprintf (stderr, "\n");
}

static void diagn_and_exit ( const char* prog_name
                           , int exit_status, int line, int e[2]
                           , const char* msg_fmt, va_list args
                           )
{
  if (exit_status != 0 && exit_status != 2)
    diagn (prog_name, line, e, msg_fmt, args);

  exit (exit_status);
}

// Wrapper for 'diagn' used in the server daemon.
static void ddiagn (int line, int e[2], const char* msg_fmt, ...)
{
  va_list args;

  va_start (args, msg_fmt);

  diagn ("so-shell-dem", line, e, msg_fmt, args);
}

// Wrapper for 'diagn_and_exit' used in the server daemon.
static void ddiagn_and_exit ( int exit_status, int line, int e[2]
                            , const char* msg_fmt, ...
                            )
{
  va_list args;

  va_start (args, msg_fmt);

  diagn_and_exit ("so-shell-dem", exit_status, line, e, msg_fmt, args);
}

// Wrapper for 'diagn' used in the server children.
static void sdiagn (int line, int e[2], const char* msg_fmt, ...)
{
  va_list args;

  va_start (args, msg_fmt);

  diagn ("so-shell-srv", line, e, msg_fmt, args);
}


// main function for the server daemon
// -----------------------------------

static int cli_sockd;

static void srv_main (void);

static const char too_many_args_msgf[] =
                    "too many arguments: %i provided, at most 1 expected"
                , unrepr_port_msgf[] =
                    "the given port represents an integer not representable "
                    "in this implementation because of overflow: %s"
                , unparsbl_port_msgf[] =
                    "the given port does not represent an integer: %s"
                , invalid_port_msgf[] =
                    "the given port is not a valid TCP port: %s"
                , socket_fail_msgf[] =
                    "failed to create the socket needed to accept client "
                    "connections"
                , bind_fail_msgf[] =
                    "failed to assign the address to the socket needed to "
                    "accept client connections"
                , listen_fail_msgf[] =
                    "failed to start listening for incoming client connections"
                , accept_fail_msgf[] =
                    "failed to accept an incoming client connection"
                , fork_fail_msgf[] =
                    "failed to create the children process needed to serve a "
                    "client"
                , close_fail_msgf[] =
                    "an error occurred while closing a file descriptor"
                , waitpid_fail_msgf[] =
                    "failed to collect the exit status of a children process"
                ;

// signal handler to collect the exit status of dead children processes
static void kill_zombies (int signum)
{
  int pid;
  int status;
  int old_errno;

  old_errno = errno;

  while (1)
  {
    pid = waitpid (-1, &status, WNOHANG);
    if (pid == -1)
    {
      if (errno == ECHILD)
        break;
      else
      {
        ddiagn (__LINE__, (int[2]){0, errno}, waitpid_fail_msgf);
        break;
      }
    }
    if (pid == 0)
      continue;
    if (WIFEXITED (status))
      printf ( "so-shell-dem: child server process with PID %i terminated "
               "normally with exit status %i\n"
             , pid, WEXITSTATUS (status)
             );
    else if (WIFSIGNALED (status))
    {
      printf ( "so-shell-dem: child server process with PID %i terminated due to"
               " a "
               #if _POSIX_C_SOURCE < 200809L
               "signal: %i", pid, WTERMSIG (status)
               #else
               "signal: %s", pid, strsignal (WTERMSIG (status)
               #endif
             );
      #ifdef WCOREDUMP
      if (WCOREDUMP (status))
        printf (" (core dumped)");
      #endif
      printf ("\n");
    }
  }

  errno = old_errno;
}

int main (int argc, char* argv[])
{
  struct sigaction kill_zombies_sa;
  struct sigaction old_sa;
  int              outcome;
  int              daemon_port = 9000;
  int              daemon_sockd;

  if (argc == 2)
  {
    char* leftover;

    errno = 0;
    daemon_port = strtol (argv[1], &leftover, 0);
    if (errno == ERANGE)
      ddiagn_and_exit (1, __LINE__, NULL, unrepr_port_msgf, argv[1]);
    else if (*leftover != 0)
      ddiagn_and_exit (1, __LINE__, NULL, unparsbl_port_msgf, argv[1]);
    else if (daemon_port > 65535 || daemon_port < 0)
      ddiagn_and_exit (1, __LINE__, NULL, invalid_port_msgf, argv[1]);
  }
  else if (argc > 2)
    ddiagn_and_exit (1, __LINE__, NULL, too_many_args_msgf, argc-1);

  kill_zombies_sa.sa_handler = kill_zombies;
  kill_zombies_sa.sa_flags   = 0;
  sigemptyset (&kill_zombies_sa.sa_mask);

  outcome = sigaction (SIGCHLD, &kill_zombies_sa, &old_sa);
  assert (outcome == 0);

  /* Open 'daemon_sockd', bind it to localhost and start listening for
     connections. */
  {
    struct sockaddr_in addr;

    daemon_sockd = socket (PF_INET, SOCK_STREAM, 0);
    if (daemon_sockd == -1)
      ddiagn_and_exit (1, __LINE__, (int[2]){0, errno}, socket_fail_msgf);

    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_family = AF_INET;
    addr.sin_port = htons (daemon_port);
    outcome = bind (daemon_sockd, (struct sockaddr*)&addr, sizeof addr);
    if (outcome == -1)
      ddiagn_and_exit (1, __LINE__, (int[2]){0, errno}, bind_fail_msgf);

    outcome = listen (daemon_sockd, 3);
    if (outcome == -1)
      ddiagn_and_exit (1, __LINE__, (int[2]){0, errno}, listen_fail_msgf);
  }

  printf ("so-shell-dem: successfully started listening for connections\n");

  while (1)
  {
    pid_t child_pid;

    {
      struct sockaddr_in cli_addr;
      unsigned           cli_addr_sz = sizeof cli_addr;

      do cli_sockd =
        accept (daemon_sockd, (struct sockaddr*)&cli_addr, &cli_addr_sz);
      while (cli_sockd == -1 && errno == EINTR);
      if (cli_sockd == -1)
      {
        ddiagn (__LINE__, (int[2]){0, errno}, accept_fail_msgf);
        continue;
      }
    }

    child_pid = fork ();
    if (child_pid == 0)
    {
      outcome = close (daemon_sockd);
      if (outcome == -1)
        sdiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);

      outcome = sigaction (SIGCHLD, &old_sa, NULL);
      assert (outcome == 0);

      srv_main ();
    }
    else if (child_pid > 0)
    {
      printf ("so-shell-dem: client accepted and served by child server process"
              " with PID %i\n", child_pid);

      outcome = close (cli_sockd);
      if (outcome == -1)
        ddiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);
    }
    else
      ddiagn_and_exit (1, __LINE__, (int[2]){0, errno}, fork_fail_msgf);
  }

  assert ("this assertion indicates an unreachable execution path" == NULL);
}


// main function for the server children
// -------------------------------------

// Wrapper for 'diagn_and_exit' used in the children of the server daemon.
static void sdiagn_and_exit ( int exit_status, int line, int e[2]
                            , const char* msg_fmt, ...
                            )
{
  va_list args;

  va_start (args, msg_fmt);

  diagn_and_exit ("so-shell-srv", exit_status, line, e, msg_fmt, args);
}

static const char recv_fail_msgf[] =
                    "failed to receive a message from the server"
                , send_fail_msgf[] =
                    "failed to send a message to the server"
                , write_input_pipe_fail_msgf[] =
                    "failed to write to the command input pipe"
                , reg_evt_fail_msgf[] =
                    "failed to register a shell event"
                , parse_fail_msgf[] =
                    "failed to parse command line: %s"
                , read_out1_fail_msgf[] =
                    "failed to read the output of the command"
                , read_out2_fail_msgf[] =
                    "failed to read the diagnostic output of the command"
                , unexpected_type_msgf[] =
                    "the server did not follow the protocol: a message of an "
                    "unexpected type with message type id '%i' was received"
                , unexpected_payload_msgf[] =
                    "the server did not follow the protocol: unexpected "
                    "payload %i for a message of type %i"
                , cli_fail_msgf[] =
                    "the client encountered a fatal error"
                , pipe_fail_msgf[] =
                    "failed to create a pipe"
                , set_cloexec_fail_msgf[] =
                    "failed to set the FD_CLOEXEC flag of a descriptor"
                , pthread_create_fail_msgf[] =
                    "failed to create the POSIX thread needed to forward "
                    "command I/O to/from the client"
                , underlivered_sig_msgf[] =
                    "the command terminated before any signal could be "
                    "delivered to it"
                , fwd_parse_err_msg_fail_msgf[] =
                    "a parser error message could have been partially or not "
                    "at all delivered to the client: an error occured while "
                    "writing to the command diagnostic output pipe"
                , parse_err_in_batch_msgf[] =
                    "aborting batch session: an invalid line in the script "
                    "was encountered"
                ;

// utility to send the last message to the client before exiting
static void goodbye_cli (int exit_status)
{
  outcome_t                outcome;
  // see the comment at the top of error reporting utilities for details
  int                      e[2];
  struct protocol_msg      msg;

  msg.type      = protocol_msg_exit_status;
  msg.payload.c = exit_status;
  outcome = protocol_send_msg (cli_sockd, msg, e);
  if (outcome == failure)
    sdiagn_and_exit (3, __LINE__, e, send_fail_msgf);
}

// wrapper for goodbye_cli that also exits
static void goodbye_cli_and_exit (int exit_status)
{
  goodbye_cli (exit_status);
  exit (exit_status);
}

// pipes used to intercept and forward the standard I/O of commands
static int input_pipe[2];
static int ord_pipe[2];
static int diagn_pipe[2];

// flag used to avoid race conditions in the signal handler that registers
// signal events (e.g. by calling 'goodbye_cli' while a message was being sent)
static int has_register_event_failed = 0;
static int has_register_event_failed_e[2];

// main function for the thread that forwards the standard I/O of commands
static void* forward_io (void* dummy)
{
  int                       outcome;
  // see the comment at the top of error reporting utilities for details
  int                       e[2];
  int                       is_input_terminated = 0;
  int                       is_ord_terminated   = 0;
  int                       is_diagn_terminated = 0;
  struct protocol_msg       msg;
  const int                 buf_capacity        = 128;
  char                      buf[buf_capacity];
  struct timeval            zero_tv             = { .tv_sec = 0, .tv_usec = 0 };

  while (!is_ord_terminated || !is_diagn_terminated  || !is_input_terminated)
  {
    fd_set fds;

    if (has_register_event_failed)
    {
      goodbye_cli (1);
      sdiagn_and_exit ( 1, __LINE__, has_register_event_failed_e
                      , reg_evt_fail_msgf);
    }

    FD_ZERO (&fds);
    FD_SET  (cli_sockd, &fds);
    if (!is_ord_terminated)
      FD_SET  (ord_pipe[0], &fds);
    if (!is_diagn_terminated)
      FD_SET  (diagn_pipe[0], &fds);
    do outcome = select (FD_SETSIZE, &fds, NULL, NULL, &zero_tv);
    while (outcome == -1 && (errno == EINTR || errno == EAGAIN));
    assert (outcome != -1);

    if (FD_ISSET (cli_sockd, &fds))
    {
      outcome = protocol_recv_msg (cli_sockd, &msg, e);
      if (outcome == failure)
        sdiagn_and_exit (3, __LINE__, e, recv_fail_msgf);
      if (msg.type == protocol_msg_eof)
      {
        if (is_input_terminated)
          sdiagn_and_exit (1, __LINE__, NULL, unexpected_type_msgf, msg.type);
        if (msg.payload.c == 0)
        {
          is_input_terminated = 1;

          outcome = close (input_pipe[1]);
          if (outcome == failure)
            sdiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);

          outcome = close (input_pipe[0]);
          if (outcome == failure)
            sdiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);
        }
        else
          sdiagn_and_exit ( 1, __LINE__, NULL
                          , unexpected_payload_msgf, msg.payload.c);
      }
      else if (msg.type == protocol_msg_input)
      {
        if (is_input_terminated)
          sdiagn_and_exit (1, __LINE__, NULL, unexpected_type_msgf, msg.type);
        outcome = write_chars ( input_pipe[1]
                              , msg.payload.s.length, msg.payload.s.head
                              );
        if (outcome == failure)
        {
          e[0] = 0;
          e[1] = errno;
          goodbye_cli (1);
          sdiagn_and_exit (1, __LINE__, e, write_input_pipe_fail_msgf);
        }
        free (msg.payload.s.head);
      }
      else if (msg.type == protocol_msg_sig)
      {
        int signum;

        signum = protocol_signum (msg.payload.c);
        if (signum != 0)
          raise (signum);
        else
          sdiagn_and_exit ( 1, __LINE__, NULL
                          , unexpected_payload_msgf, msg.payload.c);
      }
      else if (msg.type == protocol_msg_exit_status)
        sdiagn_and_exit (msg.payload.c, __LINE__, NULL, cli_fail_msgf);
      else
        sdiagn_and_exit (1, __LINE__, NULL, unexpected_type_msgf, msg.type);
    }

    if (!is_ord_terminated && FD_ISSET (ord_pipe[0], &fds))
    {
      do outcome = read (ord_pipe[0], buf, buf_capacity);
      while (outcome == -1 && errno == EINTR);
      if (outcome == 0)
      {
        is_ord_terminated = 1;
        msg.type      = protocol_msg_eof;
        msg.payload.c = 1;
        outcome = protocol_send_msg (cli_sockd, msg, e);
        if (outcome == failure)
          sdiagn_and_exit (3, __LINE__, e, send_fail_msgf);
      }
      else if (outcome == -1)
      {
        goodbye_cli (1);
        sdiagn_and_exit (1, __LINE__, (int[2]){0, errno}, read_out1_fail_msgf);
      }
      else if (outcome > 0)
      {
        msg.type             = protocol_msg_out_ord;
        msg.payload.s.length = outcome;
        msg.payload.s.head   = buf;
        outcome = protocol_send_msg (cli_sockd, msg, e);
        if (outcome == failure)
          sdiagn_and_exit (3, __LINE__, e, send_fail_msgf);
      }
    }

    if (!is_diagn_terminated && FD_ISSET (diagn_pipe[0], &fds))
    {
      do outcome = read (diagn_pipe[0], buf, buf_capacity);
      while (outcome == -1 && errno == EINTR);
      if (outcome == 0)
      {
        is_diagn_terminated = 1;
        msg.type      = protocol_msg_eof;
        msg.payload.c = 2;
        outcome = protocol_send_msg (cli_sockd, msg, e);
        if (outcome == failure)
          sdiagn_and_exit (3, __LINE__, e, send_fail_msgf);
      }
      else if (outcome == -1)
      {
        goodbye_cli (1);
        sdiagn_and_exit (1, __LINE__, (int[2]){0, errno}, read_out2_fail_msgf);
      }
      else if (outcome > 0)
      {
        msg.type             = protocol_msg_out_diagn;
        msg.payload.s.length = outcome;
        msg.payload.s.head   = buf;
        outcome = protocol_send_msg (cli_sockd, msg, e);
        if (outcome == failure)
          sdiagn_and_exit (3, __LINE__, e, send_fail_msgf);
      }
    }
  }

  return NULL;
}

// from https://www.gnu.org/software/libc/manual/html_node/Descriptor-Flags.html
/* Set the FD_CLOEXEC flag of desc if value is nonzero,
   or clear the flag if value is 0.
   Return 0 on success, or -1 on error with errno set. */
static int
set_cloexec_flag (int desc, int value)
{
  int oldflags = fcntl (desc, F_GETFD, 0);
  /* If reading the flags failed, return error indication now. */
  if (oldflags < 0)
    return oldflags;
  /* Set just the flag we want to set. */
  if (value != 0)
    oldflags |= FD_CLOEXEC;
  else
    oldflags &= ~FD_CLOEXEC;
  /* Store modified flag word in the descriptor. */
  return fcntl (desc, F_SETFD, oldflags);
}

// the state of the shell in each server children
static struct shell sh;
// Logs whether a SIGHUP was received to avoid race conditions in the SIGHUP
// signal handler.
static int was_sighup_received = 0;
// Logs whether a command has terminated while a signal was being forwarded to
// it (used to print a warning message when this happens).
static int has_child_terminated_before_sig_propagation = 0;

// signal handler for all signals which either could trigger a trap or are
// handled by the shell in a specific way
void handle_sig (int signum)
{
  outcome_t outcome;

  if (signum == SIGHUP)
    was_sighup_received = 1;

  outcome = shell_register_event (&sh, signum, has_register_event_failed_e);
  if (outcome == failure)
  {
    has_register_event_failed = 1;
    return;
  }

  if (sh.child_pid > 0 && signum != SIGUSR1 && signum != SIGUSR2)
  {
    int outcome;

    outcome = kill (sh.child_pid, signum);
    assert (outcome == 0 || (outcome == -1 && errno == ESRCH));
    if (outcome == -1 && errno == ESRCH)
      has_child_terminated_before_sig_propagation = 1;
  }
}

static void srv_main (void)
{
  const int                handled_sigs[] =
    { SIGHUP
    , SIGTERM
    , SIGINT
    , SIGQUIT
    , SIGTSTP
    , SIGCONT
    , SIGUSR1
    , SIGUSR2
    , 0 };
  struct sigaction         handle_sig_sa;
  outcome_t                outcome;
  // see the comment at the top of error reporting utilities for details
  int                      e[2];
  char*                    parse_err_msg;
  struct protocol_msg      msg;
  struct cmd               cmd;
  char*                    prompt;
  pthread_t                forward_io_pthread;
  int                      is_interactive;
  int                      was_cmd_pending = 1, is_exit_registered = 0;
  int                      is_cmd_valid = 1;

  memcpy (&sh, &shell_0, sizeof sh);

  /* Detach the process from the daemon's terminal so that signals sent to the
     process group of the daemon (e.g. from a terminal) are not sent to any
     server child. */
  outcome = setpgid (0, getpid ());
  assert (outcome == 0);

  handle_sig_sa.sa_handler = handle_sig;
  handle_sig_sa.sa_flags   = 0;
  sigemptyset (&handle_sig_sa.sa_mask);
  for (int i = 0; handled_sigs[i] != 0; i++)
  {
    outcome = sigaction (handled_sigs[i], &handle_sig_sa, NULL);
    assert (outcome == 0);
  }

  outcome = protocol_recv_msg (cli_sockd, &msg, e);
  if (outcome == failure)
    sdiagn_and_exit (3, __LINE__, e, recv_fail_msgf);
  is_interactive = msg.payload.c;

  while (1)
    {
      outcome = pipe (input_pipe);
      if (outcome == -1)
      {
        goodbye_cli (1);
        sdiagn_and_exit (1, __LINE__, (int[2]){0, errno}, pipe_fail_msgf);
      }

      outcome = set_cloexec_flag (input_pipe[1], 1);
      if (outcome == -1)
      {
        goodbye_cli (1);
        sdiagn_and_exit (1, __LINE__, (int[2]){0,errno}, set_cloexec_fail_msgf);
      }

      outcome = pipe (ord_pipe);
      if (outcome == -1)
      {
        goodbye_cli (1);
        sdiagn_and_exit (1, __LINE__, (int[2]){0, errno}, pipe_fail_msgf);
      }

      outcome = pipe (diagn_pipe);
      if (outcome == -1)
      {
        goodbye_cli (1);
        sdiagn_and_exit (1, __LINE__, (int[2]){0, errno}, pipe_fail_msgf);
      }

      if (!shell_is_cmd_pending (&sh))
      {
        was_cmd_pending = 0;

        prompt = "> ";
        msg.type             = protocol_msg_prompt;
        msg.payload.s.length = 1 + strlen (prompt);
        msg.payload.s.head   = prompt;
        outcome = protocol_send_msg (cli_sockd, msg, e);
        if (outcome == failure)
          sdiagn_and_exit (3, __LINE__, e, send_fail_msgf);

        do
        {
          fd_set         ln_ed_fds;
          struct timeval zero_tv = { .tv_sec = 0, .tv_usec = 0 };

          /* NOTE: remember to always keep this check before any check for
             'was_sighup_received'. */
          if (has_register_event_failed)
          {
            goodbye_cli (1);
            sdiagn_and_exit ( 1, __LINE__, has_register_event_failed_e
                            , reg_evt_fail_msgf);
          }

          if (has_child_terminated_before_sig_propagation)
          {
            has_child_terminated_before_sig_propagation = 0;
            sdiagn (__LINE__, NULL, underlivered_sig_msgf);
          }

          if (was_sighup_received)
          {
            if (!sh.has_exited)
              sh.has_exited = 1;
            if (!is_exit_registered)
            {
              outcome = shell_register_event (&sh, shell_evt_exit, e);
              if (outcome == failure)
              {
                goodbye_cli (1);
                sdiagn_and_exit (1, __LINE__, e, reg_evt_fail_msgf);
              }
              is_exit_registered = 1;
            }

            if (!shell_is_cmd_pending (&sh))
              goodbye_cli_and_exit (2);
          }

          if (shell_is_cmd_pending (&sh))
          {
            was_cmd_pending = 1;
            break;
          }

          FD_ZERO (              &ln_ed_fds);
          FD_SET  (cli_sockd,    &ln_ed_fds);
          do outcome = select (FD_SETSIZE, &ln_ed_fds, NULL, NULL, &zero_tv);
          while (outcome == -1 && (errno == EINTR || errno == EAGAIN));
          assert (outcome != -1);

          if (FD_ISSET (cli_sockd, &ln_ed_fds))
          {
            outcome = protocol_recv_msg (cli_sockd, &msg, e);
            if (outcome == failure)
              sdiagn_and_exit (3, __LINE__, e, recv_fail_msgf);
            if (msg.type == protocol_msg_eof)
            {
              if (!sh.has_exited)
                sh.has_exited = 1;
              if (!is_exit_registered)
              {
                outcome = shell_register_event (&sh, shell_evt_exit, e);
                if (outcome == failure)
                {
                  goodbye_cli (1);
                  sdiagn_and_exit (1, __LINE__, e, reg_evt_fail_msgf);
                }
                is_exit_registered = 1;
              }

              if (!shell_is_cmd_pending (&sh))
                goodbye_cli_and_exit (0);
            }
            else if (msg.type == protocol_msg_sig)
            {
              int signum;

              signum = protocol_signum (msg.payload.c);
              if (signum != 0)
                raise (signum);
              else
                sdiagn_and_exit ( 1, __LINE__, NULL
                                , unexpected_payload_msgf, msg.payload.c);
            }
            else if (msg.type == protocol_msg_exit_status)
              sdiagn_and_exit (msg.payload.c, __LINE__, NULL, cli_fail_msgf);
            else if (msg.type != protocol_msg_cmd_ln)
              sdiagn_and_exit (1,__LINE__,NULL, unexpected_type_msgf, msg.type);

          }
        } while (msg.type != protocol_msg_cmd_ln);

        if (!was_cmd_pending)
        {
          outcome =
            cmd_parse_line (msg.payload.s.head, &cmd, e, &parse_err_msg);
          if (outcome == failure)
          {
            if (e[1] != 0)
              sdiagn (__LINE__, e, parse_fail_msgf, msg.payload.s.head);
            is_cmd_valid = 0;
          }
          free (msg.payload.s.head);
        }
      }
      else
        was_cmd_pending = 1;
      if (was_cmd_pending)
      {
        msg.type      = protocol_msg_pending_cmd;
        msg.payload.c = 0;
        outcome = protocol_send_msg (cli_sockd, msg, e);
        if (outcome == failure)
          sdiagn_and_exit (3, __LINE__, e, send_fail_msgf);

        shell_dequeue_pending_cmd (&sh, &cmd);
      }

      outcome = pthread_create ( &forward_io_pthread, NULL, forward_io, NULL);
      if (outcome != 0)
        sdiagn_and_exit (1,__LINE__,(int[2]){0,errno},pthread_create_fail_msgf);

      if (is_cmd_valid)
      {
        outcome =
          core_exec ( &sh, &cmd
                    , (int[]) { input_pipe[0], ord_pipe[1], diagn_pipe[1] });
        //TODO do something with exit status
      }
      else
      {
        const char prefix[] = "so-shell: ";
        outcome = write_chars (diagn_pipe[1], strlen (prefix), prefix);
        if (outcome == failure)
          sdiagn (__LINE__, (int[2]){0, errno}, fwd_parse_err_msg_fail_msgf);
        outcome =
          write_chars (diagn_pipe[1], strlen (parse_err_msg), parse_err_msg);
        if (outcome == failure)
          sdiagn (__LINE__, (int[2]){0, errno}, fwd_parse_err_msg_fail_msgf);
        free (parse_err_msg);
      }

      outcome = close (ord_pipe[1]);
      if (outcome == -1)
        sdiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);

      outcome = close (diagn_pipe[1]);
      if (outcome == -1)
        sdiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);

      outcome = pthread_join (forward_io_pthread, NULL);
      assert (outcome == 0);

      if (!is_interactive && !is_cmd_valid)
      {
        goodbye_cli (1);
        sdiagn_and_exit (1, __LINE__, NULL, parse_err_in_batch_msgf);
      }

      if (has_register_event_failed)
      {
        goodbye_cli (1);
        sdiagn_and_exit ( 1, __LINE__, has_register_event_failed_e
                        , reg_evt_fail_msgf);
      }

      if (has_child_terminated_before_sig_propagation)
      {
        has_child_terminated_before_sig_propagation = 0;
        sdiagn (__LINE__, NULL, underlivered_sig_msgf);
      }

      if (!was_cmd_pending)
      {
        outcome = shell_register_event (&sh, shell_evt_debug, e);
        if (outcome == failure)
        {
          goodbye_cli (1);
          sdiagn_and_exit (1, __LINE__, e, reg_evt_fail_msgf);
        }
      }

      if (was_sighup_received)
        sh.has_exited = 1;

      if (sh.has_exited && !is_exit_registered)
      {
        outcome = shell_register_event (&sh, shell_evt_exit, e);
        if (outcome == failure)
        {
          goodbye_cli (1);
          sdiagn_and_exit (1, __LINE__, e, reg_evt_fail_msgf);
        }
        is_exit_registered = 1;
      }

      if (sh.has_exited && is_exit_registered && !shell_is_cmd_pending (&sh))
        goodbye_cli_and_exit (was_sighup_received ? 2 : 0);

      if (!was_cmd_pending && is_cmd_valid)
        cmd_free_innards (&cmd);

      outcome = close (ord_pipe[0]);
      if (outcome == -1)
        sdiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);

      outcome = close (diagn_pipe[0]);
      if (outcome == -1)
        sdiagn (__LINE__, (int[2]){0, errno}, close_fail_msgf);

      is_cmd_valid = 1;
    }

  assert ("this execution path should be unreachale" == NULL);
}
