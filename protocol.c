#include "protocol.h"

#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <arpa/inet.h>
#include "prelude.h"


// NOTE: keep all the type ids which have a string as payload greater than those
// which have a character payload; see 'msg_fst_string_payload_type'.
const char protocol_msg_none         = -1
         , protocol_msg_exit_status  = 0
         , protocol_msg_eof          = 1
         , protocol_msg_sig          = 2
         , protocol_msg_pending_cmd  = 3
         , protocol_msg_session_mode = 4
         , protocol_msg_prompt       = 5
         , protocol_msg_cmd_ln       = 6
         , protocol_msg_input        = 7
         , protocol_msg_out_ord      = 8
         , protocol_msg_out_diagn    = 9
         ;
// The lowest id of a message type with a string payload; used to allow using a
// simple comparison on the message type to determine the payload type.
static const char msg_fst_string_payload_type = 5;


// the protocol representation of signal numbers

const char protocol_sigterm = 0
         , protocol_sigint  = 1
         , protocol_sigquit = 2
         , protocol_sigtstp = 3
         , protocol_sigcont = 4
         ;

static const int protocol_map_sig_id_num[] =
  { SIGHUP
  , SIGTERM
  , SIGINT
  , SIGQUIT
  , SIGTSTP
  , SIGCONT
  };
static const int protocol_map_sig_id_num_len =
  sizeof protocol_map_sig_id_num / sizeof protocol_map_sig_id_num[0];

char protocol_sigid (int signum)
{
  for (int i = 0; i < protocol_map_sig_id_num_len; i++)
    if (protocol_map_sig_id_num[i] == signum)
      return i;
  return -1;
}

int protocol_signum (char sigid)
{
  if (sigid < protocol_map_sig_id_num_len)
    return protocol_map_sig_id_num[sigid];
  else
    return -1;
}


outcome_t protocol_recv_msg ( int sockd, struct protocol_msg* x, int est[2])
{
  outcome_t outcome;

  outcome = read_chars (sockd, 1, &(x->type));
  if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);

  if (x->type < msg_fst_string_payload_type)
  {
    outcome = read_chars (sockd, 1, &(x->payload.c));
    if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);
  }
  else
  {
    uint32_t length_serialized;

    outcome = read_chars (sockd, sizeof length_serialized,
                         (char*)&length_serialized);
    if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);
    x->payload.s.length = ntohl (length_serialized);

    x->payload.s.head = malloc (x->payload.s.length);
    if (x->payload.s.head == NULL)
      return (est[0] = __LINE__, est[1] = errno, failure);

    outcome = read_chars (sockd, x->payload.s.length, x->payload.s.head);
    if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);
  }

  return success;
}

outcome_t protocol_send_msg (int sockd, struct protocol_msg x, int est[2])
{
  outcome_t outcome;

  outcome = write_chars (sockd, 1, &(x.type));
  if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);

  if (x.type < msg_fst_string_payload_type)
  {
    outcome = write_chars (sockd, 1, &(x.payload.c));
    if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);
  }
  else
  {
    uint32_t length_serialized;

    length_serialized = htonl (x.payload.s.length);
    outcome = write_chars (sockd, sizeof length_serialized,
                          (char*)&length_serialized);
    if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);

    outcome = write_chars (sockd, x.payload.s.length, x.payload.s.head);
    if (outcome == failure) return (est[0] = __LINE__, est[1] = errno, failure);
  }

  return success;
}
