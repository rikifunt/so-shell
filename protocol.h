#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdint.h>

#include "prelude.h"

// See 'report.md' for an informal description of the protocol.

// possible values for the 'type' field of 'struct protocol_msg'
extern const char protocol_msg_session_mode
                , protocol_msg_prompt
                , protocol_msg_pending_cmd
                , protocol_msg_exit_status
                , protocol_msg_cmd_ln
                , protocol_msg_eof
                , protocol_msg_input
                , protocol_msg_out_ord
                , protocol_msg_out_diagn
                , protocol_msg_sig
                , protocol_msg_none
                ;

// the payload of a string message
struct string
  { uint32_t length
  ; char*    head
  ;};

union protocol_msg_payload
  { char          c
  ; struct string s
  ;};

// a single message of the protocol
struct protocol_msg
  { char                       type
  ; union protocol_msg_payload payload
  ;};

// functions to convert to and from signal representation of the protocol and of
// standard C
char protocol_sigid  (int);
int  protocol_signum (char);

/* Functions to exchange protocol messages over a 'SOCK_STREAM' socket using TCP
   as the underlying protocol. The first integer parameter is the descriptor of
   the socket on top of which to operate, which must already be connected
   properly with the other side.  They either send or receive a single message
   or fail with an error (stored in the caller-allocated array 'e', see below
   for details). 'protocol_recv_msg' also allocates memory for the 'head' field
   of the 'struct string' of the 'payload' field of the message if needed which
   must be freed by the caller with 'free'.

   NOTE: These functions are *NOT* thread-safe: if two messages are sent
   concurrently on the same TCP connection, it is not guaranteed the other end
   can correctly receive them as the data for the two messages may interleave
   and cause unpredictable behaviour.

   In case of errors, the first element of 'e' contains the line at which the
   error occured (probably only useful for debugging purposes), while the second
   contains the value of 'errno' which describes the error occured in the
   internal syscall that caused the failure. */

outcome_t protocol_recv_msg (int, struct protocol_msg*, int[2]);
outcome_t protocol_send_msg (int, struct protocol_msg , int[2]);


#endif // PROTOCOL_H
