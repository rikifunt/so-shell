# so-shell tasks

## Tests

- test for execution paths involving malloc failures; see
  https://stackoverflow.com/questions/1711170/unit-testing-for-failed-malloc#1711255


## Fixes

- implement a timeout when waiting for new messages to detect failures on the
  other side instead of waiting forever


## Features

- allow appending instead of overwriting when redirecting I/O

- check and use feature test macros for the different target systems (Linux,
  macOS, POSIX.7)

- support writing empty string args as `""`

- add missing features from the table in `README.md`

- add proper signal handling for non-interactive mode?

- support special names (e.g. '..' and '.'), globbing, etc. (see
  https://www.gnu.org/software/libc/manual/html_node/Pattern-Matching.html#Pattern-Matching)

- have proper prompt (complete the 'pretty_state' stub), e.g. use a prompt
  format string parameter to generate the string based on the shell state

- have coloured output


## Optimizations

- profile, use inlining, add compilation options to CFLAGS
  refs:
  - https://stackoverflow.com/questions/9428433/small-functions-defined-in-header-files-inline-or-static
  - https://gcc.gnu.org/onlinedocs/gcc/Inline.html
  - https://stackoverflow.com/questions/7762731/whats-the-difference-between-static-and-static-inline-function/7767858#7767858
  - http://clang.llvm.org/compatibility.html#inline
  - http://www.open-std.org/jtc1/sc22/wg14/www/C99RationaleV5.10.pdf
  - https://stackoverflow.com/questions/47819719/static-inline-functions-in-a-header-file


## Others

- write a configure script that checks for needed build dependencies

- determine exact version and enforce compliance with posix at compile time
