// core application logic for so-shell: commands and their evaluation
#ifndef CORE_H
#define CORE_H

#include "prelude.h"

// a single parsed command
struct cmd
  { char*  stdio_file_paths[3]
  ; int    argc
  ; char** argv
  ;};

// Parse the command line given as the first argument into the callee-allocated
// command. The fields of the command are allocated by this functions and should
// be free'd after use with 'cmd_free_innards'. In case of parse errors, the
// last argument is set to point to a dynamically allocated string which
// describes the error. In case of malloc failures, the second last argument
// (whose memory must be allocated by the caller), will contain respectively the
// associated line and 'errno' value.
outcome_t cmd_parse_line (const char*, struct cmd*, int[2], char**);

// utilities for copying and freeing the inner fields of parsed commands
// correctly. malloc failures are reported as in 'cmd_parse_line'.
outcome_t cmd_cpy_innards (struct cmd*, const struct cmd*, int[2]);
void cmd_free_innards (struct cmd*);

// a linked list of commands
struct cmds
  { struct cmd   h
  ; struct cmds* t
  ;};

// the state of a shell
struct shell
  { int           has_exited
  ; pid_t         child_pid
  // possible traps for term, int, quit, tstp, cont, hup, usr1/2, exit, debug
  ; struct cmd    traps[10]
  ; struct cmds*  pending_cmds
  ; struct cmds*  pending_cmds_last
  ;};

// the initial state of a shell; should be copied into any newly created shell
// state
extern struct shell shell_0;

extern int shell_evt_exit;
extern int shell_evt_debug;

// Register that the event given as the second argument occurred in the context
// of the given shel state and trigger any relevant trap. malloc failures are
// reported as in 'cmd_parse_line'.
outcome_t shell_register_event      (struct shell*, int, int[2]);
// Check if there is a command queued for execution by a trap.
int       shell_is_cmd_pending      (struct shell*);
// Get the next command to be executed due to a trap trigger.
void      shell_dequeue_pending_cmd (struct shell*, struct cmd*);

// Evaluate the given command in the context of the given shell state, i.e.
// perform appropriate global side effects on the calling process and update the
// given shell state. Return the exit status of the evaluated command. When an
// exit command is executed, the shell state will have an updated 'has_exited'
// flag; no calls to 'exit' are actually performed.
int core_exec (struct shell*, const struct cmd*, int[]);


#endif // CORE_H
